package Utilities.Observer;

public interface Observer
{
    public void update(Object sender, Object data);
}
