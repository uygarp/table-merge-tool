package Utilities.Lists;

import java.util.ArrayList;

public class Lists
{
    public static ArrayList<String> getCommonItems(ArrayList<String> targetList, ArrayList<String> sourceList)
    {
        if(targetList != null && sourceList != null)
        {
            ArrayList<String> resultList = new ArrayList<String>();
            
            for(String sourceString : sourceList)
            {
                if(targetList.contains(sourceString))
                {
                    resultList.add(sourceString);
                }
            }
            
            return resultList;
        }
        
        return null;
    }
    
    public static ArrayList<String> getExcessItems(ArrayList<String> targetList, ArrayList<String> sourceList)
    {
        if(targetList != null && sourceList != null)
        {
            ArrayList<String> result = new ArrayList<String>();            
            
            for(String sourceString : sourceList)
            {
                if(!targetList.contains(sourceString))
                {
                    result.add(sourceString);
                }
            }            
            
            return result;
        }
        
        return null;
    }
}
