package View.GUI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import java.util.ArrayList;

public class CustomAddRemoveCompound extends Composite
{
    private Group groupHeader;
    private RadioButtonGroup rbg;
    private AddRemoveCompound arc;

    public CustomAddRemoveCompound(Composite compound, boolean hasToolButton)
    {
        super(compound, SWT.NONE);
        init(hasToolButton);
        applyDefaults();
    }
    
    public CustomAddRemoveCompound(Composite compound, String header, boolean hasButtonGroup)
    {
        super(compound, SWT.NONE);
        init(hasButtonGroup);
        setHeader(header);
        applyDefaults();
    }

    private void init(boolean hasButtonGroup)
    {
        setLayout(new GridLayout(1, false));

        groupHeader = new Group(this, SWT.NONE);
        groupHeader.setLayout(new GridLayout(1, true));

        if(hasButtonGroup)
        {
            String[] buttonNames = {"", "", ""};
            rbg = new RadioButtonGroupCompound(groupHeader, buttonNames, false);
            rbg.addRadioButtonGroupListener(new RadioButtonGroupCompound.RadioButtonGroupListener()
            {
                @Override
                public void selectionChanged()
                {
                    updateView();
                    notifySelectionChanged();
                }
            });

            Label seperator = new Label(groupHeader, SWT.SEPARATOR | SWT.HORIZONTAL);
            seperator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        }
        else
        {
            rbg = new NullRadioButtonGroup();
        }

        arc = new AddRemoveCompound(groupHeader);
        arc.addAddRemoveCompoundListener(new AddRemoveCompound.AddRemoveCompoundListener()
        {
            @Override
            public void itemsChanged()
            {
                getParent().notifyListeners(SWT.Modify, null);
                getShell().layout(true, true);
                
                notifyItemsChanged();
            }
        });
    }

    private void applyDefaults()
    {
        String[] buttonNames = {"All", "Selected", "All But These"};
        setButtonNames(buttonNames);

        updateView();
    }

    public void setEnabled(boolean enabled)
    {
        groupHeader.setEnabled(enabled);
        rbg.setEnabled(enabled);

        arc.setEnabled((rbg.getSelection() == 0) ? false : enabled);
    }

    private void updateView()
    {
        arc.setEnabled((rbg.getSelection() == 0) ? false : true);
    }

    public void setHeader(String text)
    {
        groupHeader.setText(text);
    }

    public void setButtonNames(String[] names)
    {
        rbg.setButtonNames(names);
    }

    public void load(ArrayList<String> listData)
    {
        arc.load(listData, true);
    }
    
    public void clearLeftList()
    {
        arc.clear(true);
    }
    
    public void clearRightList()
    {
        arc.clear(false);
    }
    
    public void setLeftListHeader(String header)
    {
        arc.getLeftList().setHeader(header);
    }
    
    public void setRightListHeader(String header)
    {
        arc.getRightList().setHeader(header);
    }

    public ArrayList<String> getActionList()
    {
        return arc.getRightList().getAllList();
    }

    public int getMode()
    {
        return rbg.getSelection();
    }
    
    
    public interface CustomAddRemoveCompoundListener
    {
        public void selectionChanged();
        public void itemsChanged();
    }
    
    ArrayList<CustomAddRemoveCompoundListener> carcListenerList = new ArrayList<CustomAddRemoveCompoundListener>();
    
    public void addCustomAddRemoveCompoundListener(CustomAddRemoveCompoundListener carcListener)
    {
        carcListenerList.add(carcListener);
    }
    
    public void removeCustomAddRemoveCompoundListener(CustomAddRemoveCompoundListener carcListener)
    {
        carcListenerList.remove(carcListener);
    }
    
    private void notifySelectionChanged()
    {
        if(carcListenerList != null)
        {
            for(CustomAddRemoveCompoundListener carcListener : carcListenerList)
            {
                carcListener.selectionChanged();
            }
        }
    }
    
    private void notifyItemsChanged()
    {
        if(carcListenerList != null)
        {
            for(CustomAddRemoveCompoundListener carcListener : carcListenerList)
            {
                carcListener.itemsChanged();
            }
        }
    }
}
