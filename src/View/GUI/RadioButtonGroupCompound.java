package View.GUI;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;


public class RadioButtonGroupCompound extends Composite implements RadioButtonGroup
{
    private int buttonNumber;
    private boolean isHorizontal;
    private ArrayList<Button> buttonList;
    private String[] buttonNames;
    private int selectedIndex = 0;

    public RadioButtonGroupCompound(Composite composite, String[] buttonNames, boolean isHorizontal)
    {
        super(composite, SWT.NONE);
        if(buttonNames != null)
        {
            this.buttonNames = buttonNames;
            this.buttonNumber = buttonNames.length;
            this.isHorizontal = isHorizontal;
            buttonList = new ArrayList<Button>();
            init();
            applyDefaults();
        }
    }
    
    private int getButtonNumber()
    {
        return buttonNumber;
    }
    
    private boolean isHorizontal()
    {
        return isHorizontal;
    }
    
    private ArrayList<Button> getButtonList()
    {
        return buttonList;
    }
    
    private void init()
    {
        setLayout(new GridLayout(isHorizontal() ? getButtonNumber() : 1, false));

        SelectionListener selectionListener = new SelectionAdapter()
        {
            public void widgetSelected(SelectionEvent event)
            {
                Button button = ((Button) event.widget);
                if(button.getSelection())
                {
                    selectedIndex = getButtonList().indexOf(button);
                    
                    notifyObservers();
                }
            };
        };

        for (String text : buttonNames)
        {
            Button button = new Button(this, SWT.RADIO);
            button.setText(text);
            button.addSelectionListener(selectionListener);
            getButtonList().add(button);
        }
    }
    
    private void applyDefaults()
    {
        setSelection(0);
        setEnabled(true);
    }
    
    @Override
    public void setSelection(int selectedIndex)
    {
        if(selectedIndex < getButtonList().size())
        {
            for(Button button : getButtonList())
            {
                button.setSelection(false);
            }
            
            getButtonList().get(selectedIndex).setSelection(true);
            this.selectedIndex = selectedIndex;
        }
    }
    
    @Override
    public int getSelection()
    {
        return selectedIndex;
    }
    
    @Override
    public void setButtonNames(String[] names)
    {
        if(names.length == buttonNames.length)
        {
            for(int i = 0; i < names.length; i++)
            {
                buttonNames[i] = names[i];
                buttonList.get(i).setText(buttonNames[i]);
            }
        }
    }
    
    @Override
    public String getSelectedButtonText()
    {
        return getButtonList().get(selectedIndex).getText();
    }
    
    @Override
    public void setEnabled(boolean enabled)
    {
        for(Button button : getButtonList())
        {
            button.setEnabled(enabled);
        }
    }
    
    private ArrayList<RadioButtonGroupListener> radioButtonGroupListeners = new ArrayList<RadioButtonGroupListener>();
    
    @Override
    public void removeRadioButtonGroupListener(RadioButtonGroupListener rbglListener)
    {
        radioButtonGroupListeners.remove(rbglListener);
    }
    
    @Override
    public void addRadioButtonGroupListener(RadioButtonGroupListener rbglListener)
    {
        radioButtonGroupListeners.add(rbglListener);
    }

    private void notifyObservers()
    {
        if(radioButtonGroupListeners != null)
        {
            for(RadioButtonGroupListener rbgListener : radioButtonGroupListeners)
            {
                rbgListener.selectionChanged();
            }
        }
    }
}
