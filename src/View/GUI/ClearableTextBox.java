package View.GUI;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.graphics.Color;


public class ClearableTextBox extends Composite
{
    private final Color defWhiteColor = new Color(getDisplay(), 255, 255, 255);
    
    private Text text;
    private Button button;

    public ClearableTextBox(Composite container, int style)
    {
        super(container, style);
        init();
    }
    
    public void setEnabled(boolean enabled)
    {
        button.setEnabled(enabled);
        super.setEnabled(enabled);
    }
    
    public void setText(String string)
    {
        text.setText(string);
    }
    
    public String getText()
    {
        return text.getText();
    }
    
    private void init()
    {
        setBackground(defWhiteColor);
        
        GridLayout gridLayout = new GridLayout(2, false);
        gridLayout.verticalSpacing = 0;
        gridLayout.marginHeight = 0;
        gridLayout.horizontalSpacing = 0;
        gridLayout.marginWidth = 0;
        setLayout(gridLayout);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        
        setLayoutData(gridData);
        
        text = new Text(this, SWT.NONE);
        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        text.addKeyListener(new org.eclipse.swt.events.KeyListener()
        {
            @Override
            public void keyReleased(KeyEvent arg0)
            {
                notifyObservers();
            }
            
            @Override
            public void keyPressed(KeyEvent arg0)
            {
            }
        });
        
        button = new Button(this, SWT.NONE);
        button.setText("C");
        button.setForeground(defWhiteColor);
        button.setBackground(defWhiteColor);
        button.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseUp(MouseEvent e)
            {
                setText("");
                text.setFocus();
                notifyObservers();
            }
        });
    }
    
    public interface KeyListener
    {
        public void keyPressed();
    }
    
    private ArrayList<KeyListener> keyListenerlist = new ArrayList<KeyListener>();
    
    public void addKeyListener(KeyListener keyListener)
    {
        keyListenerlist.add(keyListener);
    }
    
    public void removeInputListener(KeyListener keyListener)
    {
        keyListenerlist.remove(keyListener);
    }
    
    private void notifyObservers()
    {
        if(keyListenerlist != null)
        {
            for(KeyListener keyListener : keyListenerlist)
            {
                keyListener.keyPressed();
            }
        }
    }
}
