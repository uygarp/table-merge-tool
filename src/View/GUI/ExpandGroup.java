package View.GUI;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;


public class ExpandGroup
{
    private final String defHeaderRows = "Rows";
    private final String defHeaderColumns = "Columns";
    private final String defHeaderLeftList = "Current";
    private final String defHeaderRightList = "Applicable";
    
    private ExtendedExpandBar eExpandBar;
    private Composite composite;
    private CustomAddRemoveCompound carcRows;
    private CustomAddRemoveCompound carcColumns;
    
    public ExpandGroup(ExtendedExpandBar eExpandBar, String header, boolean hasButtonGroup)
    {
        this.eExpandBar = eExpandBar;
        
        composite = eExpandBar.createExpandItem(header);
        composite.setLayout(new GridLayout(2, false));
        
        carcRows = new CustomAddRemoveCompound(composite, defHeaderRows, hasButtonGroup);
        carcRows.setLeftListHeader(defHeaderLeftList);
        carcRows.setRightListHeader(defHeaderRightList);
        
        carcColumns = new CustomAddRemoveCompound(composite, defHeaderColumns, hasButtonGroup);
        carcColumns.setLeftListHeader(defHeaderLeftList);
        carcColumns.setRightListHeader(defHeaderRightList);
    }
    
    public ExtendedExpandBar getBar()
    {
        return eExpandBar;
    }
    
    public Composite getComposite()
    {
        return composite;
    }
    
    public CustomAddRemoveCompound getRowsGroup()
    {
        return carcRows;
    }
    
    public CustomAddRemoveCompound getColumnsGroup()
    {
        return carcColumns;
    }
}
