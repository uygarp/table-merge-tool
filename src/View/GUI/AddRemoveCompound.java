package View.GUI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import java.util.ArrayList;

public class AddRemoveCompound extends Composite
{
    private Button buttonAdd;
    private Button buttonRemove;
    private GridData gridAddButton;
    private GridData gridRemoveButton;
    private FilterAndListCompound falcLeft;
    private FilterAndListCompound falcRight;
    private boolean equalListSize;
    
    public AddRemoveCompound(Composite container)
    {
        super(container, SWT.NONE);
        init();
        applyDefaults();
    }

    private void init()
    {
        setLayout(new GridLayout(3, false));

        falcLeft = new FilterAndListCompound(this, SWT.NONE);
        falcLeft.addSelectionListener(new FilterAndListCompound.SelectionListener()
        {
            @Override
            public void selectionChanged()
            {
                setButtonsState((getLeftList().getSelectionCount() > 0) ? ButtonsState.Add : ButtonsState.None);
            }

            @Override
            public void doubleClicked()
            {
                performAddRemoveAction(true);
            }
        });

        Composite compButtons = new Composite(this, SWT.NONE);
        compButtons.setLayout(new GridLayout(1, false));

        gridAddButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        buttonAdd = new Button(compButtons, SWT.NONE);
        buttonAdd.setLayoutData(gridAddButton);
        buttonAdd.addListener(SWT.Selection, new Listener()
        {
            public void handleEvent(Event event)
            {
                if(event.type == SWT.Selection)
                {
                    performAddRemoveAction(true);
                }
            }
        });

        gridRemoveButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        buttonRemove = new Button(compButtons, SWT.NONE);
        buttonRemove.setLayoutData(gridRemoveButton);
        buttonRemove.addListener(SWT.Selection, new Listener()
        {
            public void handleEvent(Event event)
            {
                if(event.type == SWT.Selection)
                {
                    performAddRemoveAction(false);
                }
            }
        });

        falcRight = new FilterAndListCompound(this, SWT.NONE);
        falcRight.addSelectionListener(new FilterAndListCompound.SelectionListener()
        {
            @Override
            public void selectionChanged()
            {
                setButtonsState((getRightList().getSelectionCount() > 0) ? ButtonsState.Remove : ButtonsState.None);
            }

            @Override
            public void doubleClicked()
            {
                performAddRemoveAction(false);
            }
        });
    }

    private FilterAndListCompound getList(boolean leftRight)
    {
        return leftRight ? falcLeft : falcRight;
    }

    public FilterAndListCompound getLeftList()
    {
        return falcLeft;
    }

    public FilterAndListCompound getRightList()
    {
        return falcRight;
    }

    public void load(ArrayList<String> listData, boolean leftRight)
    {
        getList(leftRight).loadList(listData);
        itemsChanged();
    }
    
    public void clear(boolean leftRight)
    {
        load(new ArrayList<String>(), leftRight);
    }

    public void setButtonWidth(int width)
    {
        gridAddButton.widthHint = width;
        gridRemoveButton.widthHint = width;
    }

    public void setAddButtonText(String text)
    {
        buttonAdd.setText(text);
    }

    public void setRemoveButtonText(String text)
    {
        buttonRemove.setText(text);
    }
    
    public boolean getEqualListSize()
    {
        return equalListSize;
    }

    public void setEqualListSize(boolean equalListSize)
    {
        this.equalListSize = equalListSize;
    }

    public void setEnabled(boolean state)
    {
        getLeftList().setEnabled(state);
        getRightList().setEnabled(state);

        setButtonsState(ButtonsState.None);

        if(!state)
        {
            getLeftList().setSelection(-1);
            getRightList().setSelection(-1);
        }
    }

    public void setFilterIgnoreCase(boolean filterIgnoreCase)
    {
        getLeftList().setFilterIgnoreCase(filterIgnoreCase);
        getRightList().setFilterIgnoreCase(filterIgnoreCase);
    }

    private void performAddRemoveAction(boolean addRemove)
    {
        if(getList(addRemove).getSelectionCount() > 0)
        {
            for (String selection : getList(addRemove).getSelection())
            {
                getList(addRemove).remove(selection);
                getList(!addRemove).add(selection);
            }
            
            itemsChanged();
        }

        setButtonsState(ButtonsState.None);
    }

    private void setButtonsState(ButtonsState state)
    {
        buttonAdd.setEnabled(state.equals(ButtonsState.Add));
        buttonRemove.setEnabled(state.equals(ButtonsState.Remove));
    }
    
    private void itemsChanged()
    {
        equalizeListSizes();
        notifyObservers();
    }
    
    private void equalizeListSizes()
    {
        if(getEqualListSize())
        {
            if(getList(true).getListWidth() < getList(false).getListWidth())
            {
                getList(true).setListWidth(getList(false).getListWidth());
            }
            else
            {
                getList(false).setListWidth(getList(true).getListWidth());
            }
            
            if(getList(true).getListHeight() < getList(false).getListHeight())
            {
                getList(true).setListHeight(getList(false).getListHeight());
            }
            else
            {
                getList(false).setListHeight(getList(true).getListHeight());
            }
        }
    }

    public void applyDefaults()
    {
        setFilterIgnoreCase(true);

        getLeftList().setHeader("Left Side");
        getRightList().setHeader("Right Side");

        setAddButtonText(">");
        setRemoveButtonText("<");

        setButtonWidth(30);
        
        setEqualListSize(true);

        setEnabled(true);
    }

    private ArrayList<AddRemoveCompoundListener> arcListenerList = new ArrayList<AddRemoveCompound.AddRemoveCompoundListener>();
    
    public void addAddRemoveCompoundListener(AddRemoveCompoundListener arcListener)
    {
        arcListenerList.add(arcListener);
    }

    public void removeAddRemoveCompoundListener(AddRemoveCompoundListener arcListener)
    {
        arcListenerList.remove(arcListener);
    }

    private void notifyObservers()
    {
        if(arcListenerList != null)
        {
            for (AddRemoveCompoundListener arcListener : arcListenerList)
            {
                arcListener.itemsChanged();
            }
        }
    }

    public interface AddRemoveCompoundListener
    {
        public void itemsChanged();
    }

    private enum ButtonsState
    {
        None, Add, Remove;
    }
}
