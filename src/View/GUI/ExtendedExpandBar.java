package View.GUI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ExpandAdapter;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Listener;

import swing2swt.layout.BorderLayout;

import java.util.ArrayList;

public class ExtendedExpandBar extends Composite
{
    private ExpandBar expandBar;
    private ArrayList<ItemState> stateList = new ArrayList<ItemState>();
    private ArrayList<ExpandItemBuffer> hidenItemList = new ArrayList<ExpandItemBuffer>();

    public ExtendedExpandBar(Composite compound, int style)
    {
        super(compound, style);
        init();
    }

    private void init()
    {
        setLayoutData(BorderLayout.CENTER);
        setLayout(new FillLayout(SWT.HORIZONTAL));

        expandBar = new ExpandBar(this, SWT.BORDER | SWT.V_SCROLL);
        expandBar.addExpandListener(new ExpandAdapter()
        {
            @Override
            public void itemExpanded(ExpandEvent e)
            {
                int index = getNormalIndex(true, getExpandItemIndex((ExpandItem)e.item));
                getStateList().set(index, ItemState.Expanded);
                notifyExpanded();
            }

            @Override
            public void itemCollapsed(ExpandEvent e)
            {
                int index = getNormalIndex(true, getExpandItemIndex((ExpandItem)e.item));
                getStateList().set(index, ItemState.Collapsed);
                notifyCollapsed();
            }
        });
    }
    
    private int getExpandItemIndex(ExpandItem expandItem)
    {
        ExpandItem[] eiList = expandBar.getItems();
        
        int index = -1;
        for(int i = 0; i < eiList.length; i++)
        {
            if(eiList[i].equals(expandItem))
            {
                index = i;
            }
        }
        
        return index;
    }

    public Composite createExpandItem(int index, String text)
    {
        if(index <= getStateList().size())
        {
            int indexVisible = getVisibilityIndex(true, index - 1);

            final Composite composite = new Composite(expandBar, SWT.NONE);

            final ExpandItem expandItem = new ExpandItem(expandBar, SWT.NONE, indexVisible + 1);
            expandItem.setText(text);
            expandItem.setControl(composite);
            
            addAutoHeightAdjustment(composite, expandItem);

            getStateList().add(index, ItemState.Collapsed);

            return composite;
        }

        return null;
    }

    private void addAutoHeightAdjustment(final Composite composite, final ExpandItem expandItem)
    {
        int INITIAL_WIDTH = 100;
        final int trimWidth = composite.computeTrim(0, 0, INITIAL_WIDTH, 100).width - INITIAL_WIDTH;
        composite.addListener(SWT.Modify, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                if(composite.isDisposed() || expandItem.isDisposed())
                {
                    return;
                }

                Point size = composite.computeSize(composite.getSize().x - trimWidth, SWT.DEFAULT);

                if(expandItem.getHeight() != size.y)
                {
                    expandItem.setHeight(size.y);
                }
            }
        });

        expandBar.addListener(SWT.Resize, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                expandBar.getDisplay().asyncExec(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(composite.isDisposed() || expandItem.isDisposed())
                        {
                            return;
                        }

                        Point size = composite.computeSize(composite.getSize().x - trimWidth, SWT.DEFAULT);

                        if(expandItem.getHeight() != size.y)
                        {
                            expandItem.setHeight(size.y);
                        }
                    }
                });
            }
        });
    }

    private void removeAutoHeightAdjustment(final Composite composite, final ExpandItem expandItem)
    {
    }
    
    public void expandAll()
    {
        for(ItemState itemState : stateList)
        {
            if(itemState.equals(ItemState.Collapsed))
            {
                itemState = ItemState.Expanded;
            }
        }
        
        for(ExpandItem item : expandBar.getItems())
        {
            item.setExpanded(true);
        }
    }
    
    public void collapseAll()
    {
        for(ItemState itemState : stateList)
        {
            if(itemState.equals(ItemState.Expanded))
            {
                itemState = ItemState.Collapsed;
            }
        }
        
        for(ExpandItem item : expandBar.getItems())
        {
            item.setExpanded(false);
        }
    }

    public Composite createExpandItem(String text)
    {
        return createExpandItem(expandBar.getItemCount(), text);
    }

    public void disposeExpandItem(int index)
    {
        if(index < getStateList().size())
        {
            if(!getStateList().get(index).equals(ItemState.Invisible))
            {
                int indexVisible = getVisibilityIndex(true, index);
                expandBar.getItem(indexVisible).dispose();
            }
            else
            {
                int indexInvisible = getVisibilityIndex(false, index);
                hidenItemList.remove(indexInvisible);
            }

            getStateList().remove(index);
        }
    }

    public void setItemVisible(int index, boolean visibled)
    {
        if((index < getStateList().size()) && (getStateList().get(index).equals(ItemState.Invisible) == visibled))
        {
            int indexVisible = getVisibilityIndex(true, index);
            int indexInvisible = getVisibilityIndex(false, index);

            if(visibled)
            {
                ExpandItemBuffer eiBuffer = hidenItemList.get(indexInvisible);
                ExpandItem expandItem = new ExpandItem(expandBar, eiBuffer.getStyle(), indexVisible + 1);
                expandItem.setText(eiBuffer.getHeader());
                expandItem.setHeight(eiBuffer.getHeight());
                expandItem.setControl(eiBuffer.getControl());
                expandItem.setExpanded(eiBuffer.getExpanded());

                addAutoHeightAdjustment((Composite) expandItem.getControl(), expandItem);
                expandItem.getControl().notifyListeners(SWT.Modify, null);

                hidenItemList.remove(indexInvisible);
                
                changeState(eiBuffer.getExpanded() ? ItemState.Expanded : ItemState.Collapsed, index);
            }
            else
            {
                ExpandItemBuffer eiBuffer = new ExpandItemBuffer(expandBar.getItem(indexVisible));
                hidenItemList.add(indexInvisible + 1, eiBuffer);

                removeAutoHeightAdjustment((Composite) expandBar.getItem(indexVisible).getControl(), expandBar.getItem(indexVisible));

                expandBar.getItem(indexVisible).setExpanded(false);
                expandBar.getItem(indexVisible).dispose();
                
                changeState(ItemState.Invisible, index);
            }
        }
    }
    
    public enum ItemState
    {
        Invisible,
        Collapsed,
        Expanded
    }
    
    public ItemState getItemState(int index)
    {
        if(index < getStateList().size())
        {
            return stateList.get(index);
        }
        
        return ItemState.Invisible;
    }
    
    public ArrayList<ItemState> getStateList()
    {
        return stateList;
    }

    public int getVisibilityIndex(boolean state, int index)
    {
        int count = -1;

        if(index < stateList.size())
        {
            for (int i = 0; i <= index; i++)
            {
                if(state)
                {
                    count += !stateList.get(i).equals(ItemState.Invisible) ? 1 : 0;
                }
                else
                {
                    count += stateList.get(i).equals(ItemState.Invisible) ? 1 : 0;
                }
            }
        }

        return count;
    }
    
    private int getNormalIndex(boolean state, int visibilityIndex)
    {
        int index = -1;
        if(visibilityIndex > -1 && visibilityIndex < stateList.size())
        {
            int count = -1;
            for (index = 0; index <= stateList.size(); index++)
            {
                count += (state ^ stateList.get(index).equals(ItemState.Invisible)) ? 1 : 0;
                
                if(count == visibilityIndex)
                {
                    return index;
                }
            }
        }

        return index;
    }

    public void changeState(ItemState state, int index)
    {
        if(index < stateList.size())
        {
            stateList.set(index, state);
        }
    }

    public class ExpandItemBuffer
    {
        private int style;
        private String header;
        private Control control;
        private int height;
        private boolean expanded;

        public ExpandItemBuffer(ExpandItem expandItem)
        {
            if(expandItem != null)
            {
                setStyle(expandItem.getStyle());
                setHeader(expandItem.getText());
                setHeight(expandItem.getHeight());
                setControl(expandItem.getControl());
                setExpanded(expandItem.getExpanded());
            }
        }

        public String getHeader()
        {
            return header;
        }

        public void setHeader(String header)
        {
            this.header = header;
        }

        public Control getControl()
        {
            return control;
        }

        public void setControl(Control control)
        {
            this.control = control;
        }

        public int getStyle()
        {
            return style;
        }

        public void setStyle(int style)
        {
            this.style = style;
        }

        public int getHeight()
        {
            return height;
        }

        public void setHeight(int height)
        {
            this.height = height;
        }

        public boolean getExpanded()
        {
            return expanded;
        }

        public void setExpanded(boolean expanded)
        {
            this.expanded = expanded;
        }
    }
    
    public interface ExtendedExpandBarListener
    {
        public void itemExpanded();
        public void itemCollapsed();
    }
    
    ArrayList<ExtendedExpandBarListener> eebListenerList = new ArrayList<ExtendedExpandBarListener>();
    
    public void addExtendedExpandBarListener(ExtendedExpandBarListener eebListener)
    {
        eebListenerList.add(eebListener);
    }
    
    public void removeExtendedExpandBarListener(ExtendedExpandBarListener eebListener)
    {
        eebListenerList.remove(eebListener);
    }
    
    private void notifyExpanded()
    {
        if(eebListenerList != null)
        {
            for(ExtendedExpandBarListener eebListener : eebListenerList)
            {
                eebListener.itemExpanded();
            }
        }
    }
    
    private void notifyCollapsed()
    {
        if(eebListenerList != null)
        {
            for(ExtendedExpandBarListener eebListener : eebListenerList)
            {
                eebListener.itemExpanded();
            }
        }
    }  
}
