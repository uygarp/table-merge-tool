package View.GUI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;

import java.util.ArrayList;
import java.util.Collections;

public class FilterAndListCompound extends Composite
{
    private final int defaultMinListWidth = 70;
    private final int defaultMinListHeight = 20;
    private final int defaultMaxListWidth = 300;
    private final int defaultMaxListHeight = 200;
    private Group groupHeader;
    private ClearableTextBox text;
    private List list;
    private ArrayList<String> listData;
    private GridData grid;
    private boolean filterIgnoreCase;
    private int minListWidth;
    private int minListHeight;
    private int maxListWidth;
    private int maxListHeight;
    private ArrayList<SelectionListener> selectionListenerList = new ArrayList<SelectionListener>();

    public FilterAndListCompound(Composite container, int style)
    {
        super(container, style);
        init();
        applyDefaults();
    }

    private void init()
    {
        setLayout(new GridLayout(1, false));

        groupHeader = new Group(this, SWT.NONE);
        groupHeader.setLayout(new GridLayout(1, true));

        text = new ClearableTextBox(groupHeader, SWT.BORDER);
        text.addKeyListener(new ClearableTextBox.KeyListener()
        {
            @Override
            public void keyPressed()
            {
                filterList();
                notifySelectionChange();
            }
        });

        grid = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        list = new List(groupHeader, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
        list.setLayoutData(grid);
        list.addListener(SWT.Selection, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                if(event.type == SWT.Selection)
                {
                    notifySelectionChange();
                }
            }
        });
        list.addListener(SWT.DefaultSelection, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                if(event.type == SWT.DefaultSelection)
                {
                    notifyDoubleClick();
                }
            }
        });

        addListener(SWT.Resize, new Listener()
        {
            @Override
            public void handleEvent(Event event)
            {
                adjustSize();
            }
        });

        listData = new ArrayList<String>();
    }

    private void adjustSize()
    {
        if(listData.size() > 0)
        {
            int widthComputed = getComputedMaxWidth();
            int heightComputed = getComputedMaxHeight();

            int width = 0;
            width = ((getMinListWidth() != -1) && (widthComputed < getMinListWidth())) ? getMinListWidth() : widthComputed;
            width = ((getMaxListWidth() != -1) && (width > getMaxListWidth())) ? getMaxListWidth() : width;

            int height = 0;
            height = ((getMinListHeight() != -1) && (heightComputed < getMinListHeight())) ? getMinListHeight() : heightComputed;
            height = ((getMaxListHeight() != -1) && (height > getMaxListHeight())) ? getMaxListHeight() : height;

            setListWidth(width);
            setListHeight(height);
        }
    }

    private int getComputedMaxWidth()
    {
        GC gc = new GC(list);

        int maxSize = 0;
        for (String item : listData)
        {
            int size = 0;
            for (char ch : item.toCharArray())
            {
                size += gc.getCharWidth(ch);
            }

            maxSize = (maxSize < size) ? size : maxSize;
        }

        return maxSize;
    }

    private int getComputedMaxHeight()
    {
        int itemCount = listData.size();
        GC gc = new GC(list);

        return gc.getFontMetrics().getHeight() * itemCount;
    }

    public void applyDefaults()
    {
        filterIgnoreCase = true;

        setHeader("Header");
        setListWidth(defaultMinListWidth);
        setListHeight(defaultMinListHeight);
        setMinListWidth(defaultMinListWidth);
        setMinListHeight(defaultMinListHeight);
        setMaxListWidth(defaultMaxListWidth);
        setMaxListHeight(defaultMaxListHeight);

        setEnabled(true);
    }
    
    public void loadList(ArrayList<String> dataArray)
    {
        if(dataArray.size() >= 0)
        {
            listData.clear();
            list.removeAll();

            for (String header : dataArray)
            {
                listData.add(header);
            }

            filterList();
        }
    }

    public ArrayList<String> getAllList()
    {
        return listData;
    }

    public void setHeader(String header)
    {
        groupHeader.setText(header);
    }

    public void setListWidth(int width)
    {
        grid.widthHint = width;
    }

    public int getListWidth()
    {
        return grid.widthHint;
    }
    
    public void setListHeight(int height)
    {
        grid.heightHint = height;
    }

    public int getListHeight()
    {
        return grid.heightHint;
    }

    public int getMinListWidth()
    {
        return minListWidth;
    }

    public void setMinListWidth(int minListWidth)
    {
        this.minListWidth = minListWidth;
    }

    public int getMinListHeight()
    {
        return minListHeight;
    }

    public void setMinListHeight(int minListHeight)
    {
        this.minListHeight = minListHeight;
    }

    public int getMaxListWidth()
    {
        return maxListWidth;
    }

    public void setMaxListWidth(int maxListWidth)
    {
        this.maxListWidth = maxListWidth;
    }

    public int getMaxListHeight()
    {
        return maxListHeight;
    }

    public void setMaxListHeight(int maxListHeight)
    {
        this.maxListHeight = maxListHeight;
    }

    public void add(String data)
    {
        listData.add(data);
        filterList();
    }

    public void remove(String data)
    {
        listData.remove(data);
        filterList();
    }
    
    public int getVerticalScrollIndex()
    {
        int index = 0;
        
        ScrollBar scrollBar = list.getVerticalBar();
        if(scrollBar != null)
        {
            index = scrollBar.getSelection();
        }
        
        return index;
    }
    
    public void setVerticalScrollIndex(int index)
    {
        ScrollBar scrollBar = list.getVerticalBar();
        if(scrollBar != null)
        {
            scrollBar.setSelection(index + getVisibleRowCount());
            
            list.setSelection(index + getVisibleRowCount() + 1);
            list.setSelection(index);
            
            list.deselectAll();
        }
    }
    
    private int getVisibleRowCount()
    {
        return getListHeight() / (new GC(list)).getFontMetrics().getHeight() - 1;
    }

    public void setEnabled(boolean enabled)
    {
        groupHeader.setEnabled(enabled);
        list.setEnabled(enabled);
        text.setEnabled(enabled);

        if(!enabled)
        {
            list.setSelection(-1);
        }
    }

    public void setSelection(int index)
    {
        list.setSelection(index);
    }

    public String[] getSelection()
    {
        return list.getSelection();
    }

    public int getSelectionCount()
    {
        return list.getSelectionCount();
    }

    public void setFilterIgnoreCase(boolean filterIgnoreCase)
    {
        this.filterIgnoreCase = filterIgnoreCase;
    }

    private void filterList()
    {
        Collections.sort(listData, String.CASE_INSENSITIVE_ORDER);

        String[] filteredList = filter(text.getText(), listData);

        int scrollIndex = getVerticalScrollIndex();
        list.setItems(filteredList);        
        
        notifyListeners(SWT.Resize, null);
        
        setVerticalScrollIndex(scrollIndex);
    }

    private String[] filter(String filterText, ArrayList<String> dataList)
    {
        String[] filteredStringArray = null;
        ArrayList<String> filteredArray = null;

        if(filterText.equals(""))
        {
            filteredArray = dataList;
        }
        else
        {
            filteredArray = new ArrayList<String>();

            for (String data : dataList)
            {
                String dataCompare = filterIgnoreCase ? data.toLowerCase() : data;
                String filterTextCompare = filterIgnoreCase ? filterText.toLowerCase() : filterText;

                if(dataCompare.contains(filterTextCompare))
                {
                    filteredArray.add(data);
                }
            }
        }

        filteredStringArray = new String[filteredArray.size()];

        for (int i = 0; i < filteredArray.size(); i++)
        {
            filteredStringArray[i] = filteredArray.get(i);
        }

        return filteredStringArray;
    }

    public void addSelectionListener(SelectionListener selectionListener)
    {
        selectionListenerList.add(selectionListener);
    }

    public void removeSelectionListener(SelectionListener selectionListener)
    {
        selectionListenerList.remove(selectionListener);
    }

    private void notifySelectionChange()
    {
        for (SelectionListener selectionListener : selectionListenerList)
        {
            selectionListener.selectionChanged();
        }
    }

    private void notifyDoubleClick()
    {
        for (SelectionListener selectionListener : selectionListenerList)
        {
            selectionListener.doubleClicked();
        }
    }

    public interface SelectionListener
    {
        public void selectionChanged();

        public void doubleClicked();
    }
}
