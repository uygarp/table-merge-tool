package View.GUI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Text;

import java.util.ArrayList;

public class FileOpenCompound extends Composite
{
    private final static String defButtonName = "File";
    private final static int sizeButton = 75;
    private final static int size = 800;
    private Button button;
    private Text text;
    private GridData gdButton;
    private GridData gdText;
    private FileDialog dialogFileOpen;
    private String selectedFilePath;
    ArrayList<FileOpenListener> foListenerList = new ArrayList<FileOpenCompound.FileOpenListener>();

    FileOpenCompound(Composite composite)
    {
        super(composite, SWT.NONE);
        init();
        applyDefaults();
    }

    private void init()
    {
        setLayout(new GridLayout(2, false));
        setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

        gdButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        button = new Button(this, SWT.NONE);
        button.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseDown(MouseEvent arg0)
            {
                fileOpen();
            }
        });
        button.setLayoutData(gdButton);

        gdText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        text = new Text(this, SWT.BORDER);
        text.setEditable(false);
        text.setLayoutData(gdText);

        dialogFileOpen = new FileDialog(this.getShell(), SWT.OPEN);
        selectedFilePath = "";
    }

    private void applyDefaults()
    {
        setButtonText(defButtonName);
        setButtonSize(sizeButton, getButtonSize().y);
        setSize(size, getSize().y);
    }

    private void fileOpen()
    {
        String filePath = dialogFileOpen.open();
        filePath = (filePath == null) ? "" : filePath;

        text.setText(filePath);
        setSelectedFilePath(filePath);

        if(!filePath.equals(""))
        {
            notifyObsevers();
        }
    }
    
    public void setButtonImage(String imageFile)
    {
        button.setImage(new Image(getDisplay(), getClass().getClassLoader().getResourceAsStream(imageFile)));
    }

    public String getSelectedFilePath()
    {
        return selectedFilePath;
    }

    private void setSelectedFilePath(String selectedFilePath)
    {
        this.selectedFilePath = selectedFilePath;
    }

    public void setFilterNames(String[] filterNames)
    {
        dialogFileOpen.setFilterNames(filterNames);
    }

    public void setFilterExtensions(String[] filterExtensions)
    {
        dialogFileOpen.setFilterExtensions(filterExtensions);
    }

    public void setFilterIndex(int index)
    {
        dialogFileOpen.setFilterIndex(index);
    }

    public void setEnabled(boolean enabled)
    {
        button.setEnabled(enabled);
        text.setEnabled(enabled);
    }

    public void setButtonText(String text)
    {
        button.setText(text);
    }

    public void setSize(int width, int height)
    {
        if(width > gdButton.widthHint)
        {
            gdText.widthHint = width - gdButton.widthHint;
        }
        else
        {
            gdButton.widthHint = width;
            gdText.widthHint = 0;
        }

        gdButton.heightHint = height;
        gdText.heightHint = height;
    }

    public Point getSize()
    {
        int height = (gdText.heightHint > gdButton.heightHint) ? gdText.heightHint : gdButton.heightHint;

        return new Point(gdText.widthHint + gdButton.widthHint, height);
    }

    public void setButtonSize(int width, int height)
    {
        gdButton.widthHint = width;
        gdButton.heightHint = height;
    }

    public Point getButtonSize()
    {
        return new Point(gdButton.widthHint, gdButton.heightHint);
    }

    public void setTextSize(int width, int height)
    {
        gdText.widthHint = width;
        gdText.heightHint = height;
    }

    public Point getTextSize()
    {
        return new Point(gdText.widthHint, gdText.heightHint);
    }

    public void addFileOpenListener(FileOpenListener foListener)
    {
        foListenerList.add(foListener);
    }

    public void removeFileOpenListener(FileOpenListener foListener)
    {
        foListenerList.remove(foListener);
    }

    private void notifyObsevers()
    {
        if(foListenerList != null)
        {
            for(FileOpenListener foListener : foListenerList)
            {
                foListener.fileSelected();
            }
        }
    }

    public interface FileOpenListener
    {
        public void fileSelected();
    }
}
