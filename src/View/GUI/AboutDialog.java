package View.GUI;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Shell;

import swing2swt.layout.BorderLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;

import View.ProgramInfo;

import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.widgets.Label;

public class AboutDialog
{
    String dialogHeader = "About";
    String defIconTMT = "images/tmt.png";
    
    private Shell about;
    private StyledText textHeader;
    private StyledText textDescription;
    
    public AboutDialog(Shell shell)
    {
        about = new Shell(shell);
        about.setSize(400, 275);
        about.setText(dialogHeader);
        
        Rectangle bounds = about.getDisplay().getPrimaryMonitor().getBounds();
        Rectangle rect = about.getBounds();        
        int x = bounds.x + (bounds.width - rect.width) / 2;
        int y = bounds.y + (bounds.height - rect.height) / 2;        
        about.setLocation(x, y);
        about.setLayout(new BorderLayout(0, 0));
        
        Composite composite = new Composite(about, SWT.NONE);
        composite.setLayoutData(BorderLayout.SOUTH);
        composite.setLayout(new FormLayout());
        
        Button btnClose = new Button(composite, SWT.CENTER);
        FormData fd_btnClose = new FormData();
        fd_btnClose.right = new FormAttachment(100, -3);
        fd_btnClose.bottom = new FormAttachment(100);
        fd_btnClose.top = new FormAttachment(100, -25);
        fd_btnClose.left = new FormAttachment(100, -70);
        btnClose.setLayoutData(fd_btnClose);
        btnClose.setText("Close");
        btnClose.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                about.close();
            }
        });
        
        Composite composite_1 = new Composite(about, SWT.NONE);
        composite_1.setLayoutData(BorderLayout.CENTER);
        composite_1.setLayout(new FillLayout(SWT.HORIZONTAL));
        
        textDescription = new StyledText(composite_1, SWT.BORDER | SWT.WRAP);
        textDescription.setEnabled(false);
        textDescription.setEditable(false);
        
        Composite composite_2 = new Composite(about, SWT.NONE);
        composite_2.setLayoutData(BorderLayout.NORTH);
        composite_2.setLayout(new GridLayout(2, false));
        
        Composite composite_3 = new Composite(composite_2, SWT.NONE);
        composite_3.setLayout(new FillLayout(SWT.HORIZONTAL));
        composite_3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        
        Label lblNewLabel = new Label(composite_3, SWT.NONE);
        lblNewLabel.setImage(new Image(shell.getDisplay(), getClass().getClassLoader().getResourceAsStream(defIconTMT)));
        lblNewLabel.setAlignment(SWT.CENTER);
        
        textHeader = new StyledText(composite_2, SWT.BORDER);
        textHeader.setEnabled(false);
        textHeader.setEditable(false);
        GridData gd_textHeader = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_textHeader.heightHint = 75;
        textHeader.setLayoutData(gd_textHeader);
        
        about.open( );
        
        initializeFields();
    }
    
    private void initializeFields()
    {
        String headerField = ProgramInfo.Name + "\n\n"
                + "Version : " + ProgramInfo.Version + "\n"
                + ProgramInfo.Owner + ", " + ProgramInfo.Year ;
        textHeader.setText(headerField);
        
        FontData data = textHeader.getFont().getFontData()[0];
        Font font = new Font(about.getDisplay(), data.getName(), data.getHeight() * 8 / 5, data.getStyle());
        
        StyleRange[] styles = new StyleRange[4];
        styles[0] = new StyleRange();
        styles[0].font = font;
        styles[0].fontStyle = SWT.BOLD;
        styles[1] = new StyleRange();
        styles[1].fontStyle = SWT.BOLD;
        styles[2] = new StyleRange();
        styles[2].fontStyle = SWT.NORMAL;
        styles[3] = new StyleRange();
        styles[3].fontStyle = SWT.BOLD;
        
        int phLen = ProgramInfo.Name.length();
        int verLen = ProgramInfo.Version.length();
        int oyLen = ProgramInfo.Owner.length() + ProgramInfo.Year.length() + 2;
        
        int[] ranges = new int[] {0, phLen, phLen + 2, 10, phLen + 12, verLen, phLen + 13 + verLen, oyLen};
        
        textHeader.setStyleRanges(ranges, styles);
        
        String descField = "Description : " + ProgramInfo.Description + "\n\n"
                + "Author : " + ProgramInfo.Author + " / " + ProgramInfo.Email;
        textDescription.setText(descField);
        
        styles = new StyleRange[4];
        styles[0] = new StyleRange();
        styles[0].fontStyle = SWT.BOLD;
        styles[1] = new StyleRange();
        styles[1].fontStyle = SWT.NORMAL;
        styles[2] = new StyleRange();
        styles[2].fontStyle = SWT.BOLD;
        styles[3] = new StyleRange();
        styles[3].fontStyle = SWT.NORMAL;
        
        int desLen = ProgramInfo.Description.length();
        int autLen = ProgramInfo.Author.length();
        int emLen = ProgramInfo.Email.length();
        
        ranges = new int[] {0, 14, 14, desLen, desLen + 15, 9, desLen + 24, autLen + 3 + emLen};
        
        textDescription.setStyleRanges(ranges, styles);        
    }
}