package View.GUI;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;

import View.View;
import View.GUI.ExtendedExpandBar.ItemState;
import View.GUI.FileOpenCompound.FileOpenListener;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.FillLayout;

import swing2swt.layout.BorderLayout;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.ProgressBar;


public class GUI extends View
{
    private static final int defFilterIndex = 0;
    private static final String defApplicationName = "Translations Merge Tool";
    private static final String defStrTarget = "Target";
    private static final String defStrSource = "Source";
    private static final String defStrUpdate = "Update";
    private static final String defStrAdd = "Add";
    private static final String defStrRemove = "Remove";
    private static final String defStrAbout = "About";
    private static final String defStrCheck = "Check";
    private static final String defStrAction = "Apply";
    private static final String defIconFileOpen = "images/file_open.png";
    private static final String defIconAbout = "images/about.png";
    private static final String defIconCheck = "images/check.png";
    private static final String defIconApply = "images/apply.png";
    private static final String defIconTmtSmall = "images/tmt_small.png";
    
    private static Shell shell;
    
    private FileOpenCompound foTarget;
    private FileOpenCompound foSource;    
    
    private ExtendedExpandBar eExpandBar;
    
    private static ExpandGroup egUpdate;
    private static ExpandGroup egAdd;
    private static ExpandGroup egRemove;
    
    private Label labelStatus;
    private Button buttonAction;
    private Button buttonCheck;
    private Button buttonAbout;
    private ProgressBar progressBar;
    
    public GUI()
    {
        createControls();
    }
    
    private void createControls()
    {
        shell = new Shell();
        shell.setMinimumSize(new Point(1200, 600));
        shell.setSize(1200, 700);
        shell.setText(defApplicationName);
        shell.setLayout(new BorderLayout(0, 0));
        shell.setImage(new Image(shell.getDisplay(), getClass().getClassLoader().getResourceAsStream(defIconTmtSmall)));
        
        createNorthRegion();
        createMiddleRegion();
        createBottomRegion();
    }
    
    private void createNorthRegion()
    {
        Composite comTop = new Composite(shell, SWT.BORDER);
        comTop.setLayoutData(BorderLayout.NORTH);
        comTop.setLayout(new GridLayout(1, false));
        
        foTarget = createFileOpen(comTop, defStrTarget);
        foTarget.addFileOpenListener(new FileOpenListener()
        {
            @Override
            public void fileSelected()
            {
                setTargetFile();
            }
        });

        foSource = createFileOpen(comTop, defStrSource);
        foSource.addFileOpenListener(new FileOpenListener()
        {
            @Override
            public void fileSelected()
            {
                setSourceFile();
            }
        });
    }
    
    private FileOpenCompound createFileOpen(Composite composite, String buttonText)
    {
        FileOpenCompound fileOpen = new FileOpenCompound(composite);
        fileOpen.setButtonImage(defIconFileOpen);
        fileOpen.setButtonText(buttonText);
        
        return fileOpen;
    }
    
    private void createMiddleRegion()
    {
        Composite comMiddle = new Composite(shell, SWT.NONE);
        comMiddle.setLayoutData(BorderLayout.CENTER);
        comMiddle.setLayout(new FillLayout(SWT.HORIZONTAL));
        
        eExpandBar = new ExtendedExpandBar(shell, SWT.NONE);
        eExpandBar.addExtendedExpandBarListener(new ExtendedExpandBar.ExtendedExpandBarListener()
        {
            @Override
            public void itemExpanded()
            {
                selectionsChanged();
            }

            @Override
            public void itemCollapsed()
            {
                selectionsChanged();
            }
        });
                
        egUpdate = new ExpandGroup(eExpandBar, defStrUpdate, true);
        egAdd = new ExpandGroup(eExpandBar, defStrAdd, false);
        egRemove = new ExpandGroup(eExpandBar, defStrRemove, false);
        
        addCustomAddRemoveCompoundListener(egUpdate);
        addCustomAddRemoveCompoundListener(egAdd);
        addCustomAddRemoveCompoundListener(egRemove);        
    }
    
    private void addCustomAddRemoveCompoundListener(ExpandGroup expandGroup)
    {
        expandGroup.getRowsGroup().addCustomAddRemoveCompoundListener(new CustomAddRemoveCompound.CustomAddRemoveCompoundListener()
        {
            @Override
            public void selectionChanged()
            {
                selectionsChanged();
            }

            @Override
            public void itemsChanged()
            {
                selectionsChanged();
            }
        });

        expandGroup.getColumnsGroup().addCustomAddRemoveCompoundListener(new CustomAddRemoveCompound.CustomAddRemoveCompoundListener()
        {
            @Override
            public void selectionChanged()
            {
                selectionsChanged();
            }

            @Override
            public void itemsChanged()
            {
                selectionsChanged();
            }
        });
    }
    
    private void createBottomRegion()
    {
        Composite comBottom = new Composite(shell, SWT.BORDER);
        comBottom.setLayoutData(BorderLayout.SOUTH);
        comBottom.setLayout(new GridLayout(2, false));
        
        labelStatus = new Label(comBottom, SWT.NONE);
        labelStatus.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        buttonAbout = new Button(comBottom, SWT.NONE);
        buttonAbout.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        buttonAbout.setImage(new Image(shell.getDisplay(), getClass().getClassLoader().getResourceAsStream(defIconAbout)));
        buttonAbout.setText(defStrAbout);
        buttonAbout.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                new AboutDialog(shell);
            }
        });
        
        progressBar = new ProgressBar(comBottom, SWT.NONE);
        progressBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        buttonCheck = new Button(comBottom, SWT.NONE);
        buttonCheck.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        buttonCheck.setImage(new Image(shell.getDisplay(), getClass().getClassLoader().getResourceAsStream(defIconCheck)));
        buttonCheck.setText(defStrCheck);
        buttonCheck.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                buttonCheckPressed();
            }
        });
        new Label(comBottom, SWT.NONE);
        buttonAction = new Button(comBottom, SWT.NONE);
        buttonAction.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        buttonAction.setImage(new Image(shell.getDisplay(), getClass().getClassLoader().getResourceAsStream(defIconApply)));
        buttonAction.setText(defStrAction);
        buttonAction.addSelectionListener(new SelectionAdapter()
        {
            @Override
            public void widgetSelected(SelectionEvent arg0)
            {
                buttonActionPressed();
            }
        });
    }
    
    private void buttonActionPressed()
    {
        action();
    }
    
    private void buttonCheckPressed()
    {
        check();
    }
    
    private void clearActionLists()
    {
        egUpdate.getRowsGroup().clearRightList();
        egUpdate.getColumnsGroup().clearRightList();
        
        egAdd.getRowsGroup().clearRightList();
        egAdd.getColumnsGroup().clearRightList();
        
        egRemove.getRowsGroup().clearRightList();
        egRemove.getColumnsGroup().clearRightList();
    }

    @Override
    public void initUpdateRows(ArrayList<String> list)
    {
        egUpdate.getRowsGroup().load(list);
    }

    @Override
    public void initUpdateColumns(ArrayList<String> list)
    {
        egUpdate.getColumnsGroup().load(list);
    }

    @Override
    public void initAddRows(ArrayList<String> list)
    {
        egAdd.getRowsGroup().load(list);
    }

    @Override
    public void initAddColumns(ArrayList<String> list)
    {
        egAdd.getColumnsGroup().load(list);
    }

    @Override
    public void initRemoveRows(ArrayList<String> list)
    {
        egRemove.getRowsGroup().load(list);
    }

    @Override
    public void initRemoveColumns(ArrayList<String> list)
    {
        egRemove.getColumnsGroup().load(list);
    }

    @Override
    public void open()
    {
        shell.open();
        shell.layout();
        
        Display display = Display.getDefault();
        
        while(!shell.isDisposed())
        {
            if(!display.readAndDispatch())
            {
                display.sleep();
            }
        }
    }

    @Override
    public ArrayList<String> getUpdateRowList()
    {
        return egUpdate.getRowsGroup().getActionList();
    }

    @Override
    public ArrayList<String> getUpdateColumnList()
    {
        return egUpdate.getColumnsGroup().getActionList();
    }

    @Override
    public ArrayList<String> getAddRowList()
    {
        return egAdd.getRowsGroup().getActionList();
    }

    @Override
    public ArrayList<String> getAddColumnList()
    {
        return egAdd.getColumnsGroup().getActionList();
    }

    @Override
    public ArrayList<String> getRemoveRowList()
    {
        return egRemove.getRowsGroup().getActionList();
    }

    @Override
    public ArrayList<String> getRemoveColumnList()
    {
        return egRemove.getColumnsGroup().getActionList();
    }

    @Override
    public void viewUpdatestatusMessage(String message)
    {
        labelStatus.setText(message);
    }

    @Override
    public String getTargetFileName()
    {
        return foTarget.getSelectedFilePath();
    }

    @Override
    public String getSourceFileName()
    {
        return foSource.getSelectedFilePath();
    }

    @Override
    public void viewUpdateInit()
    {
        foTarget.setEnabled(true);
        foSource.setEnabled(false);

        eExpandBar.setItemVisible(0, false);
        eExpandBar.setItemVisible(1, false);
        eExpandBar.setItemVisible(2, false);
        
        viewUpdatestatusMessage("");
        progressBar.setVisible(false);
        
        enableActionButtons(false);
    }

    @Override
    public void viewUpdateTargetFileSelected()
    {
        clearActionLists();
        
        foSource.setEnabled(true);
        
        eExpandBar.setItemVisible(2, true);        
    }

    @Override
    public void viewUpdateSourceFileSelected()
    {
        foTarget.setEnabled(true);
        foSource.setEnabled(true);
        
        eExpandBar.setItemVisible(0, true);
        eExpandBar.setItemVisible(1, true);
        eExpandBar.setItemVisible(2, true);
        
        enableActionButtons(false);
    }    

    @Override
    public void viewUpdateSetEnabledAction(boolean enabled)
    {
        viewUpdatestatusMessage("");
        progressBar.setVisible(false);
        
        enableActionButtons(enabled);
    }
    
    private void enableActionButtons(boolean enabled)
    {
        buttonAction.setEnabled(enabled);
        buttonCheck.setEnabled(enabled);
    }
    
    @Override
    public void viewUpdateActionPerforming()
    {
        foTarget.setEnabled(false);
        foSource.setEnabled(false);
        
        eExpandBar.setEnabled(false);
        
        viewUpdatestatusMessage("");
        progressBar.setVisible(true);
        
        enableActionButtons(false);
    }
    
    @Override
    public void viewUpdateActionPerformed()
    {
        foTarget.setEnabled(true);
        foSource.setEnabled(true);
        
        eExpandBar.setEnabled(true);
        
        enableActionButtons(false);        
    }

    @Override
    public UpdateMode getUpdateRowsMode()
    {
        return getUpdateMode(egUpdate.getRowsGroup());
    }

    @Override
    public UpdateMode getUpdateColumnsMode()
    {
        return getUpdateMode(egUpdate.getColumnsGroup());
    }
    
    private UpdateMode getUpdateMode(CustomAddRemoveCompound carc)
    {
        if(carc.getMode() < UpdateMode.values().length)
        {
            return UpdateMode.values()[carc.getMode()];
        }
        
        return UpdateMode.All;
    }

    @Override
    public void initFileExtentions(String[][] fileExtentionList)
    {
        String[] fileFilterExtentions = getFileFilterExtensions(fileExtentionList);
        String[] fileFilterNames = getFileFilterNames(fileExtentionList);

        foTarget.setFilterExtensions(fileFilterExtentions);
        foTarget.setFilterNames(fileFilterNames);
        foTarget.setFilterIndex(defFilterIndex);

        foSource.setFilterExtensions(fileFilterExtentions);
        foSource.setFilterNames(fileFilterNames);
        foSource.setFilterIndex(defFilterIndex);
    }
    
    private String[] getFileFilterExtensions(String[][] fileExtentionList)
    {
        String[] fileFilterExtentions = new String[fileExtentionList.length + 1];
        
        fileFilterExtentions[0] = "";
        
        for(int i = 0; i < fileExtentionList.length; i++)
        {
            fileFilterExtentions[i + 1] = "*." + fileExtentionList[i][0] + ";";
            fileFilterExtentions[0] += fileFilterExtentions[i + 1];
        }

        return fileFilterExtentions;
    }
    
    private String[] getFileFilterNames(String[][] fileExtentionList)
    {
        String[] fileFilterNames = new String[fileExtentionList.length + 1];
        
        fileFilterNames[0] = "All Supported";
        
        for(int i = 0; i < fileExtentionList.length; i++)
        {
            fileFilterNames[i + 1] = fileExtentionList[i][1] + " (*." + fileExtentionList[i][0] + ")";
        }

        return fileFilterNames;
    }

    @Override
    public boolean getUpdateExpanded()
    {
        return eExpandBar.getItemState(0).equals(ItemState.Expanded);
    }

    @Override
    public boolean getAddExpanded()
    {
        return eExpandBar.getItemState(1).equals(ItemState.Expanded);
    }

    @Override
    public boolean getRemoveExpanded()
    {
        return eExpandBar.getItemState(2).equals(ItemState.Expanded);
    }

    @Override
    public void viewUpdateOperationPersentage(int percentage)
    {
        progressBar.setSelection(percentage);
    }
}
