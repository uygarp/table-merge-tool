package View.GUI;

public interface RadioButtonGroup
{
    public void setSelection(int selectedIndex);
    
    public int getSelection();
    
    public void setButtonNames(String[] names);
    
    public String getSelectedButtonText();
    
    public void setEnabled(boolean enabled);
    
    public interface RadioButtonGroupListener
    {
        public void selectionChanged();
    }
    
    public void addRadioButtonGroupListener(RadioButtonGroupListener rbglListener);
    
    public void removeRadioButtonGroupListener(RadioButtonGroupListener rbglListener);
}
