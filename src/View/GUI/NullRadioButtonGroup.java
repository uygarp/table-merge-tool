package View.GUI;

public class NullRadioButtonGroup implements RadioButtonGroup
{
    @Override
    public void setSelection(int selectedIndex)
    {
    }

    @Override
    public int getSelection()
    {
        return 1;
    }

    @Override
    public void setButtonNames(String[] names)
    {
    }

    @Override
    public String getSelectedButtonText()
    {
        return "";
    }

    @Override
    public void setEnabled(boolean enabled)
    {
    }

    @Override
    public void addRadioButtonGroupListener(RadioButtonGroupListener rbglListener)
    {
    }

    @Override
    public void removeRadioButtonGroupListener(RadioButtonGroupListener rbglListener)
    {
    }
}
