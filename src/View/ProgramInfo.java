package View;

public interface ProgramInfo
{
    String Name = "TMT (Translations Merge Tool)";
    String Version = "V0.1";
    String Owner = "VESTEL Electronics";
    String Year = "2015";
    
    String Description = "This is a program which makes the following processes on user-selected target file with the information from the user-selected source file:\n"
            + "\t - Updating all/selected translations\n"
            + "\t - Adding user-selected string-ID�s ancolng-ID�s\n"
            + "\t - Removing user-selected string-ID�s ancolng-ID�s";

    String Author = "Uygar Poyraz [uygarp]";
    String Email = "uygar.poyraz@vestel.com.tr";
    String repoSite = "";
}
