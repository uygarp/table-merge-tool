package View;


import java.util.ArrayList;

import Controller.Command.*;
import Model.TableOperation.*;
import Utilities.Observer.Observable;
import Utilities.Observer.Observer;

public abstract class View implements Observer, Observable
{
    private ArrayList<Observer> observerList;
    
    public enum UpdateMode
    {
        All,
        Selected,
        Ignore,
        None
    }
    
    public View()
    {
        observerList = new ArrayList<Observer>();
    }
    
    public abstract void initFileExtentions(String[][] fileExtentionList);
    public abstract void viewUpdateInit();
    public abstract void viewUpdateTargetFileSelected();
    public abstract void viewUpdateSourceFileSelected();
    public abstract void viewUpdateSetEnabledAction(boolean enabled);
    
    public abstract void open();
    public abstract void initUpdateRows(ArrayList<String> list);
    public abstract void initUpdateColumns(ArrayList<String> list);
    public abstract void initAddRows(ArrayList<String> list);
    public abstract void initAddColumns(ArrayList<String> list);
    public abstract void initRemoveRows(ArrayList<String> list);
    public abstract void initRemoveColumns(ArrayList<String> list);
    public abstract void viewUpdatestatusMessage(String message);
    public abstract void viewUpdateOperationPersentage(int operationNo);
    public abstract void viewUpdateActionPerforming();
    public abstract void viewUpdateActionPerformed();
    
    public abstract String getTargetFileName();
    public abstract String getSourceFileName();
    
    public abstract boolean getUpdateExpanded();
    public abstract boolean getAddExpanded();
    public abstract boolean getRemoveExpanded();
    public abstract UpdateMode getUpdateRowsMode();
    public abstract UpdateMode getUpdateColumnsMode();
    public abstract ArrayList<String> getUpdateRowList();
    public abstract ArrayList<String> getUpdateColumnList();
    public abstract ArrayList<String> getAddRowList();
    public abstract ArrayList<String> getAddColumnList();
    public abstract ArrayList<String> getRemoveRowList();
    public abstract ArrayList<String> getRemoveColumnList();
    
    protected void action()
    {
        notifyObservers(new CommandAction());
    }
    
    protected void check()
    {
        notifyObservers(new CommandCheck());
    }
    
    public void setTargetFile()
    {
        notifyObservers(new CommandTargetFileSelected());
    }
    
    public void setSourceFile()
    {
        notifyObservers(new CommandSourceFileSelected());
    }
    
    public void selectionsChanged()
    {
        notifyObservers(new CommandViewSelectionsChanged());
    }
        
    @Override
    public void update(Object sender, Object data)
    {
        if(sender instanceof Update)
        {
            modelUpdate(data);
        }
    }
    
    private void modelUpdate(Object data)
    {
        if(data instanceof String[])
        {
            String[] list = (String[]) data;
            viewUpdatestatusMessage("Updating [ " + list[0] + " : " + list[1] + " ] = "  + list[2]);
        }
    }

    @Override
    public void registerObserver(Observer observer)
    {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer)
    {
        observerList.remove(observer);
    }

    @Override
    public void notifyObservers(Object sender, Object data)
    {
        for(Observer observer : observerList)
        {
            observer.update(sender, data);
        }
    }
    
    public void notifyObservers(Object data)
    {
        notifyObservers(this, data);
    }
}
