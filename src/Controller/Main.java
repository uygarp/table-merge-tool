package Controller;

import Model.Model;
import View.*;
import View.CLI.*;
import View.GUI.*;

public class Main
{
    public static void main(String[] args)
    {
        Model model = new Model();
        View view = ((args == null) || (args.length <= 0)) ? new GUI() : new CLI();
        Controller controller = new Controller(view, model);

        controller.run();
    }
}
