package Controller.State;

import View.View.UpdateMode;

public class StateFileSelected extends StateInit
{
    public void viewSelectionsChanged()
    {
        boolean changed = false;
        
        if(getView().getUpdateExpanded())
        {
            if(getView().getUpdateRowsMode().equals(UpdateMode.All))
            {
                changed |= (getView().getUpdateColumnsMode().equals(UpdateMode.All)) || (getView().getUpdateColumnList().size() > 0);
            }
            else if(getView().getUpdateColumnsMode().equals(UpdateMode.All))
            {
                changed |= (getView().getUpdateRowsMode().equals(UpdateMode.All)) || (getView().getUpdateRowList().size() > 0);
            }
            else
            {
                changed |= (getView().getUpdateRowList().size() > 0) && (getView().getUpdateColumnList().size() > 0);
            }
        }
        
        if(getView().getAddExpanded())
        {
            changed |= (getView().getAddRowList().size() > 0) || (getView().getAddColumnList().size() > 0);
        }
        
        if(getView().getRemoveExpanded())
        {
            changed |= (getView().getRemoveRowList().size() > 0) || (getView().getRemoveColumnList().size() > 0); 
        }
        
        setState(changed ? new StateReadyToAction() : this);
        getView().viewUpdateSetEnabledAction(changed);
    }
}
