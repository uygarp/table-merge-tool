package Controller.State;

import java.util.ArrayList;

import Model.TableOperation.TableOperationDecorator;

public class StateActionPerforming extends StateReadyToAction
{
    public void statusMessage(Object sender, Object data)
    {
        if(sender instanceof TableOperationDecorator)
        {
            getView().viewUpdatestatusMessage(getMessage(data));
            getView().viewUpdateOperationPersentage(100 * getOperationNo(data) / getNumOfOperation());
        }
    }
    
    private String getMessage(Object paramsData)
    {
        @SuppressWarnings("unchecked")
        ArrayList<String> params = (ArrayList<String>) paramsData;
        String doneText = userParam(params.get(0));
        String colID = userParam(params.get(1));
        String strID = userParam(params.get(2));
        
        return doneText + " [" + colID + ", " + strID + "]";
    }
    
    private String userParam(String param)
    {
        return param.equals("") ? "*" : param;
    }
    
    private int getOperationNo(Object paramsData)
    {
        @SuppressWarnings("unchecked")
        ArrayList<String> params = (ArrayList<String>) paramsData;
        
        return Integer.parseInt(params.get(3));
    }
    
    public void actionCompleted()
    {
        setState(new StateFileSelected());
        getController().targetSelected();
        getView().viewUpdatestatusMessage("Completed!");
        getView().viewUpdateActionPerformed();
    }
}
