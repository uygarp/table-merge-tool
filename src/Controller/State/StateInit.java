package Controller.State;

import java.util.ArrayList;

import Utilities.Lists.Lists;

public class StateInit extends State
{
    public void targetSelected()
    {
        if(getModel().setTargetTable(getView().getTargetFileName()) ==  1)
        {
            getView().initRemoveRows(getModelTargetTable().getRowHeaders());
            getView().initRemoveColumns(getModelTargetTable().getColumnHeaders());
            getView().viewUpdateTargetFileSelected();
            setState(new StateFileSelected());
            
            sourceSelected();
        }
        else
        {
            // Notify Error to user.
            // Set view
        }
    }
    
    public void sourceSelected()
    {
        if(getModel().setSourceTable(getView().getSourceFileName()) ==  1)
        {
            ArrayList<String> targetRows = getModelTargetTable().getRowHeaders();
            ArrayList<String> targetColumns = getModelTargetTable().getColumnHeaders();
            ArrayList<String> sourceRows = getModelSourceTable().getRowHeaders();
            ArrayList<String> sourceColumns = getModelSourceTable().getColumnHeaders();
            
            ArrayList<String> updateRowList = Lists.getCommonItems(targetRows, sourceRows);
            ArrayList<String> updateColumnList = Lists.getCommonItems(targetColumns, sourceColumns);
            ArrayList<String> addRowList = Lists.getExcessItems(targetRows, sourceRows);
            ArrayList<String> addColumnList = Lists.getExcessItems(targetColumns, sourceColumns);
            
            getView().initUpdateRows(updateRowList);
            getView().initUpdateColumns(updateColumnList);
            getView().initAddRows(addRowList);
            getView().initAddColumns(addColumnList);
            getView().viewUpdateSourceFileSelected();
            
            viewSelectionsChanged();
        }
        else
        {
            // Notify Error to user.
            // Set view
        }
    }
}
