package Controller.State;

import java.util.ArrayList;

import Utilities.Lists.Lists;
import View.View.UpdateMode;

public class StateReadyToAction extends StateFileSelected
{
    private static int numberOfOperation;
    
    public void action()
    {
        performAction(true);
    }
    
    public void check()
    {
        performAction(false);
    }
    
    private void performAction(boolean writeFile)
    {
        getView().viewUpdateActionPerforming();
        setState(new StateActionPerforming());
        getLists();
        getModel().attachOperations(writeFile);
        setNumOfOperation(getModel().getTotalOperationNumber());
        getModel().action();
        getController().actionCompleted();
    }
    
    public int getNumOfOperation()
    {
        return numberOfOperation;
    }

    public void setNumOfOperation(int numberOfOperation)
    {
        StateReadyToAction.numberOfOperation = numberOfOperation;
    }
    
    private void getLists()
    {
        if(getView().getUpdateExpanded())
        {
            UpdateMode updateMode = getView().getUpdateRowsMode();
            ArrayList<String> modelTargetList = Lists.getCommonItems(getModelTargetTable().getRowHeaders(), getModelSourceTable().getRowHeaders());
            ArrayList<String> viewTargetList = getView().getUpdateRowList();
            ArrayList<String> updateRowList = getUpdateList(updateMode, modelTargetList, viewTargetList);            
            
            updateMode = getView().getUpdateColumnsMode();
            modelTargetList = Lists.getCommonItems(getModelTargetTable().getColumnHeaders(), getModelSourceTable().getColumnHeaders());
            viewTargetList = getView().getUpdateColumnList();
            ArrayList<String> updateColumnList = getUpdateList(updateMode, modelTargetList, viewTargetList);
            
            getModel().setUpdateRowList(updateRowList);            
            getModel().setUpdateColumnList(updateColumnList);
        }
        else
        {
            getModel().setUpdateRowList(emptyList());
            getModel().setUpdateColumnList(emptyList());
        }
        
        boolean isAddExpanded = getView().getAddExpanded();
        getModel().setAddRowList(isAddExpanded ? getView().getAddRowList() : emptyList());
        getModel().setAddColumnsList(isAddExpanded ? getView().getAddColumnList() : emptyList());
        
        boolean isRemoveExpanded = getView().getRemoveExpanded();
        getModel().setRemoveRowList(isRemoveExpanded ? getView().getRemoveRowList() : emptyList());
        getModel().setRemoveColumnsList(isRemoveExpanded ? getView().getRemoveColumnList() : emptyList());
    }
    
    private ArrayList<String> getUpdateList(UpdateMode updateMode, ArrayList<String> modelTargetList, ArrayList<String> viewTargetList)
    {
        ArrayList<String> updateRowList = new ArrayList<String>();
        
        switch(updateMode)
        {
            case All:
                updateRowList = modelTargetList;
            break;
            case Selected:
                updateRowList = viewTargetList;
            break;
            case Ignore:
                updateRowList = Lists.getExcessItems(viewTargetList, modelTargetList); 
            break;
            case None:
            default:
            break;
        }
        
        return updateRowList;
    }
    
    private ArrayList<String> emptyList()
    {
        return new ArrayList<String>();
    }
}
