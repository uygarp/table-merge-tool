package Controller.State;

import Model.Model;
import Model.TableOperation.Table;
import View.View;
import Controller.Controller;

public abstract class State
{
    private Controller controller;
    
    protected Controller getController()
    {
        return controller;
    }

    public void setController(Controller controller)
    {
        this.controller = controller;
    }
    
    public void setState(State state)
    {
        getController().setState(state);
    }
    
    protected View getView()
    {
        return getController().getView();
    }
    
    protected Model getModel()
    {
        return getController().getModel();
    }
    
    protected Table getModelTargetTable()
    {
        return getController().getModel().getTargetTable();
    }
    
    protected Table getModelSourceTable()
    {
        return getController().getModel().getSourceTable();
    }
    
    public void init()
    {
        getController().getView().initFileExtentions(getController().getModel().getFileExtentions());
        getController().getView().viewUpdateInit();
    }
    
    public void viewSelectionsChanged()
    {        
    }
    
    public void targetSelected()
    {
    }
    
    public void sourceSelected()
    {
    }
    
    public void updateRowsChanged()
    {
    }
    
    public void updateColumnsChanged()
    {
    }
    
    public void updateModeChanged()
    {
    }    
    
    public void action()
    {
    }
    
    public void actionCompleted()
    {   
    }
    
    public void check()
    {
    }
    
    public void statusMessage(Object sender, Object data)
    {   
    }
}