package Controller;

import Controller.State.State;
import Controller.State.StateInit;
import Controller.Command.Command;

import Model.Model;

import Utilities.Observer.Observer;

import View.*;

public class Controller implements Observer
{
    private View view;
    private Model model;
    private State state;

    public Controller(View view, Model model)
    {
        this.view = view;
        this.model = model;

        view.registerObserver(this);
        model.registerObserver(this);

        setState(new StateInit());
        init();
    }

    public View getView()
    {
        return view;
    }

    public Model getModel()
    {
        return model;
    }

    protected State getState()
    {
        return state;
    }

    public void setState(State state)
    {
        this.state = state;
        state.setController(this);
    }

    public void run()
    {
        getView().open();
    }

    @Override
    public void update(Object sender, Object data)
    {
        if(sender instanceof View)
        {
            viewUpdated(data);
        }
        else
        {
            modelUpdated(sender, data);
        }
    }

    private void viewUpdated(Object data)
    {
        executeCommand((Command) data);
    }

    private void modelUpdated(Object sender, Object data)
    {
        getState().statusMessage(sender, data);
    }

    private void executeCommand(Command command)
    {
        command.executeFor(this);
    }

    public void init()
    {
        getState().init();
    }

    public void targetSelected()
    {
        getState().targetSelected();
    }

    public void sourceSelected()
    {
        getState().sourceSelected();
    }

    public void updateRowsChanged()
    {
        getState().updateRowsChanged();
    }

    public void updateColumnsChanged()
    {
        getState().updateColumnsChanged();
    }

    public void updateModeChanged()
    {
        getState().updateModeChanged();
    }

    public void viewSelectionsChanged()
    {
        getState().viewSelectionsChanged();
    }

    public void action()
    {
        getState().action();
    }

    public void actionCompleted()
    {
        getState().actionCompleted();
    }

    public void check()
    {
        getState().check();
    }
}
