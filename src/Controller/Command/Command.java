package Controller.Command;

import Controller.Controller;

public abstract class Command
{
    public abstract void executeFor(Controller controller);
}
