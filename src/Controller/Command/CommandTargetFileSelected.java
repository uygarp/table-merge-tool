package Controller.Command;

import Controller.Controller;

public class CommandTargetFileSelected extends Command
{
    @Override
    public void executeFor(Controller controller)
    {
        controller.targetSelected();
    }
}
