package Controller.Command;

import Controller.Controller;

public class CommandAction extends Command
{
    @Override
    public void executeFor(Controller controller)
    {
        controller.action();
    }
}
