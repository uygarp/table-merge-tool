package Controller.Command;

import Controller.Controller;

public class CommandCheck extends Command
{
    @Override
    public void executeFor(Controller controller)
    {
        controller.check();
    }
}
