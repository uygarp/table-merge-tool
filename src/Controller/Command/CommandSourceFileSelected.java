package Controller.Command;

import Controller.Controller;

public class CommandSourceFileSelected extends Command
{
    @Override
    public void executeFor(Controller controller)
    {
        controller.sourceSelected();
    }
}
