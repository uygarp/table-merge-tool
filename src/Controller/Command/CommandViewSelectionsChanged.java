package Controller.Command;

import Controller.Controller;

public class CommandViewSelectionsChanged extends Command
{
    @Override
    public void executeFor(Controller controller)
    {
        controller.viewSelectionsChanged();
    }
}
