package Model.FileAccessor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CdxFormatter
{
    private String fileName;

    public CdxFormatter(String fileName)
    {
        this.fileName = fileName;
    }

    public void format(ArrayList<String> orderList)
    {
        StringBuffer strBuffer = getStringBuffer();
        strBuffer = replaceFirst(strBuffer, " standalone=\"no\"", "");
        strBuffer = order(strBuffer, orderList);
        strBuffer = transformNewLineChars(strBuffer);
        writeStringBuffer(strBuffer);
    }

    private StringBuffer replaceFirst(StringBuffer strBuffer, String oldString, String newString)
    {
        int startIndex = strBuffer.indexOf(oldString);
        int endIndex = startIndex + oldString.length();
        strBuffer = strBuffer.replace(startIndex, endIndex, newString);

        return strBuffer;
    }

    private StringBuffer order(StringBuffer strBuffer, ArrayList<String> orderList)
    {
        ArrayList<String> colAttOrder = new ArrayList<String>(Arrays.asList(CdxConstants.attCode, CdxConstants.attName, CdxConstants.attNative, CdxConstants.attEnabled));
        strBuffer = sort(strBuffer, CdxConstants.tagColumn, colAttOrder);

        ArrayList<String> strAttOrder = new ArrayList<String>(Arrays.asList(CdxConstants.attName));
        for (String strAtt : orderList)
        {
            strAttOrder.add(strAtt);
        }
        strBuffer = sort(strBuffer, CdxConstants.tagString, strAttOrder);

        return strBuffer;
    }

    private StringBuffer transformNewLineChars(StringBuffer strBuffer)
    {
        String str = strBuffer.toString();
        str = str.replaceAll("&#13", "&#xD");
        str = str.replaceAll("&#10", "&#xA");
        strBuffer = new StringBuffer(str);

        return strBuffer;
    }

    private StringBuffer sort(StringBuffer strBuffer, String tagName, ArrayList<String> orderList)
    {
        String tagStart = "<" + tagName;
        String tagEnd = "/>";

        int startIndex = 0;
        int endIndex = 0;

        while((endIndex < strBuffer.length()) && (startIndex >= 0))
        {
            startIndex = strBuffer.indexOf(tagStart, endIndex);
            if(startIndex >= 0)
            {
                endIndex = strBuffer.indexOf(tagEnd, startIndex);
                if(endIndex > 0)
                {
                    String oldAttrListStr = strBuffer.substring(startIndex + tagStart.length(), endIndex);
                    ArrayList<XmlAttribute> attributes = getAttributeList(oldAttrListStr);
                    String newAttrListStr = "";

                    for (String attName : orderList)
                    {
                        String attValue = getValueOfAttribute(attributes, attName);
                        if(!attValue.equals(""))
                        {
                            newAttrListStr += (" " + attName + "=" + attValue);
                        }
                    }
                    newAttrListStr += " ";

                    strBuffer.replace(startIndex + tagStart.length(), endIndex, newAttrListStr);
                }
            }
        }

        return strBuffer;
    }

    private String getValueOfAttribute(ArrayList<XmlAttribute> attList, String name)
    {
        String value = "";

        for (XmlAttribute attribute : attList)
        {
            if(attribute.getName().equals(name))
            {
                value = attribute.getValue();
                break;
            }
        }

        return value;
    }

    private ArrayList<XmlAttribute> getAttributeList(String attListStr)
    {
        ArrayList<XmlAttribute> attributeList = new ArrayList<XmlAttribute>();

        Pattern attPattern = Pattern.compile("([\\w:\\-]+)(\\s*=\\s*(\"(.*?)\"|'(.*?)'|([^ ]*))|(\\s+|\\z))");
        Matcher matcher = attPattern.matcher(attListStr);

        while(matcher.find())
        {
            String attName = matcher.group(1);
            String attValue = matcher.group(3);
            attributeList.add(new XmlAttribute(attName, attValue));
        }

        return attributeList;
    }

    private StringBuffer getStringBuffer()
    {
        StringBuffer buffer = null;

        try
        {
            InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(inputStreamReader);

            buffer = new StringBuffer();

            String line;
            while((line = reader.readLine()) != null)
            {
                buffer.append(line);
                buffer.append(System.getProperty("line.separator"));
            }

            reader.close();
        }
        catch(FileNotFoundException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        catch(IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return buffer;
    }

    private void writeStringBuffer(StringBuffer buffer)
    {
        try
        {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

            bufferedWriter.write(buffer.toString());
            bufferedWriter.flush();
            bufferedWriter.close();
        }
        catch(IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
