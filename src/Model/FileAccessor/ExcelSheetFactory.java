package Model.FileAccessor;

public class ExcelSheetFactory
{
	private ExcelSheet sheet;
	
	public ExcelSheetFactory(String fileName, int sheetNo)
	{
		String extension = getFileExtention(fileName);
		
		if(extension.equalsIgnoreCase("xls"))
		{
			sheet = new HssfExcelSheet(fileName, sheetNo);
		}
		else if(extension.equalsIgnoreCase("xlsx"))
		{
			sheet = new XssfExcelSheet(fileName, sheetNo);
		}
		else
		{
			System.out.println("Error on File Type!!!");
		}
	}
	
	private String getFileExtention(String fileName)
	{
		String extension = "";

		int i = fileName.lastIndexOf('.');
		int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));

		if (i > p)
		{
		    extension = fileName.substring(i+1);
		}
		
		return extension;
	}
	
	public ExcelSheet getSheet()
	{
		return sheet;
	}
}
