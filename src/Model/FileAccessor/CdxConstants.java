package Model.FileAccessor;

public interface CdxConstants
{
    public static String tagDictionary = "Dictionary";
    public static String tagColumns = "Columns";
    public static String tagRows = "Rows";
    public static String tagColumn = "column";
    public static String tagString = "string";
    public static String attDefault = "default";
    public static String attName = "name";
    public static String attCode = "code";
    public static String attNative = "native";
    public static String attEnabled = "enabled";
    public static String valEnabled = "True";
}
