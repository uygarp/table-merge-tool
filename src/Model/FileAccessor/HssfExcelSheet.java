package Model.FileAccessor;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.IOException;

public class HssfExcelSheet extends ExcelSheet
{
	public HssfExcelSheet(String fileName, int sheetNo)
    {
        super(fileName);

        try
        {
            workbook = new HSSFWorkbook(inFile);
            sheet = workbook.getSheetAt(sheetNo);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
