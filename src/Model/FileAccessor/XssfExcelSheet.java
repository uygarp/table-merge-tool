package Model.FileAccessor;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;

public class XssfExcelSheet extends ExcelSheet {
    
    public XssfExcelSheet(String fileName, int sheetNo)
    {
        super(fileName);

        try
        {
            workbook = new XSSFWorkbook(inFile);
            sheet = workbook.getSheetAt(sheetNo);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
