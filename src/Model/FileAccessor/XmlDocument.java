package Model.FileAccessor;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class XmlDocument
{
    protected DocumentBuilder documentBuilder;
    protected Document document;
    protected Transformer transformer;
    protected StreamResult streamResult;
    protected String fileName;

    public XmlDocument()
    {
        try
        {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            transformer = TransformerFactory.newInstance().newTransformer();
        }
        catch(ParserConfigurationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch(TransformerConfigurationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch(TransformerFactoryConfigurationError e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }    
    
    public void createDocument(String fileName)
    {
        document = documentBuilder.newDocument();
        streamResult = new StreamResult(new File(fileName));
        this.fileName = fileName;
    }
    
    public void openDocument(String fileName)
    {
        try
        {
            document = documentBuilder.parse(fileName);
            streamResult = new StreamResult(new File(fileName));
            this.fileName = fileName;
        }
        catch(SAXException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch(IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public Element createRootElement(String name)
    {
        return createElement(document, name);
    }
    
    public void addTextNode(Element element, String value)
    {
        element.appendChild(document.createTextNode(value));
    }
    
    public Element createElement(Node parent, String elementName)
    {
        Element element = document.createElement(elementName);
        parent.appendChild(element);
        return element;
    }
    
    public Element createElementWithText(Element parent, String name, String text)
    {
        Element element = createElement(parent, name);
        addTextNode(element, text);
        return element;
    }
    
    public Element getRootElement(String name)
    {
        return getElement(name, document);
    }
    
    public Element getElement(String name, Node parent)
    {
        NodeList nodeList = parent.getChildNodes();
        if(nodeList != null)
        {
            for(int i = 0; i < nodeList.getLength(); i++)
            {
                Node node = nodeList.item(i);
                if(node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals(name))
                {
                    return (Element)node;
                }
            }
        }
        
        return null;
    }
    
    public ArrayList<Element> getChildElements(Node parent)
    {
        if(parent != null)
        {
            NodeList nodeList = parent.getChildNodes();
            if(nodeList.getLength() > 0)
            {
                ArrayList<Element> childList = new ArrayList<Element>();
                for(int i = 0; i < nodeList.getLength(); i++)
                {
                    if(nodeList.item(i) instanceof Element)
                    {
                        childList.add((Element)nodeList.item(i));
                    }
                }            
                
                return childList;
            }
        }
        
        return null;
    }
       
    private void cleanUpWhiteSpaces(Node node)
    {
        NodeList childNodes = node.getChildNodes();
        for (int n = childNodes.getLength() - 1; n >= 0; n--)
        {
            Node child = childNodes.item(n);
            switch(child.getNodeType())
            {
                case Node.ELEMENT_NODE:
                    cleanUpWhiteSpaces(child);
                    break;
                case Node.TEXT_NODE:
                    String trimmedValue = child.getNodeValue().trim();
                    if(trimmedValue.length() == 0)
                    {
                        node.removeChild(child);
                    }
                    else
                    {
                        child.setNodeValue(trimmedValue);
                    }
                    break;
                default:
                    break;
            }
        }
    }
    
    private void formatXML()
    {
        cleanUpWhiteSpaces(document.getFirstChild());
        document.setXmlStandalone(false);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
    }
    
    private void formatFile()
    {
        try
        {
            Path path = Paths.get(fileName);
            String content = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
            content = content.replaceAll("/>", " />");
            Files.write(path, content.getBytes(StandardCharsets.UTF_8));
        }
        catch(IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void saveDocument()
    {
        formatXML();
        try
        {
            transformer.transform(new DOMSource(document), streamResult);
        }
        catch(TransformerException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        formatFile();
    }
}
