package Model.FileAccessor;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;

public abstract class ExcelSheet
{
    protected Workbook workbook;
    protected Sheet sheet;
    protected FileInputStream inFile;
    private String fileName;

    public ExcelSheet(String fileName)
    {
        try
        {
            this.fileName = fileName;
            inFile = new FileInputStream(new File(fileName));
        }
        catch(FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public int getColumnNumber()
    {
        return sheet.getRow(0).getPhysicalNumberOfCells();
    }

    public int getRowNumber()
    {
        return sheet.getPhysicalNumberOfRows();
    }

    private Cell getCell(int columnIndex, int rowIndex)
    {
        Row row = sheet.getRow(rowIndex);

        if(row != null)
        {
            return row.getCell(columnIndex);
        }

        return null;
    }

    public String getCellString(int columnIndex, int rowIndex)
    {
        String value = "";

        Cell cell = getCell(columnIndex, rowIndex);

        if((cell != null) && (cell.getCellType() == Cell.CELL_TYPE_STRING))
        {
            value = cell.getStringCellValue();
        }

        return value;
    }

    public void setCellString(int columnIndex, int rowIndex, String value)
    {
        Cell cell = getCell(columnIndex, rowIndex);

        if(cell == null)
        {
            Row row = sheet.getRow(rowIndex);
            if(row != null)
            {
                cell = row.createCell(columnIndex);
            }
        }

        cell.setCellValue(value);
    }

    public CellStyle getCellStyle(int columnIndex, int rowIndex)
    {
        CellStyle cellStyle = null;

        Row row = sheet.getRow(rowIndex);

        if(row != null)
        {
            Cell cell = row.getCell(columnIndex);

            if(cell != null)
            {
                cellStyle = cell.getCellStyle();
            }
        }

        return cellStyle;
    }

    public CellStyle copyCellStyle(int columnIndex, int rowIndex)
    {
        CellStyle cellStyle = null;

        Row row = sheet.getRow(rowIndex);

        if(row != null)
        {
            Cell cell = row.getCell(columnIndex);

            if(cell != null)
            {
                // cellStyle = cell.getCellStyle();
                cellStyle = workbook.createCellStyle();
                cellStyle.cloneStyleFrom(cell.getCellStyle());
            }
        }

        return cellStyle;
    }

    public CellStyle copyCellStyle(CellStyle fromStyle)
    {
        CellStyle toStyle = null;

        if(fromStyle != null)
        {
            toStyle = workbook.createCellStyle();
            toStyle.cloneStyleFrom(fromStyle);
        }

        return toStyle;
    }

    public void setBold(CellStyle cellStyle, boolean enable)
    {
        Font font = workbook.createFont();
        font.setBoldweight(enable ? Font.BOLDWEIGHT_BOLD : Font.BOLDWEIGHT_NORMAL);
        cellStyle.setFont(font);
    }

    public void setBackgroundColor(CellStyle cellStyle, short color)
    {
        cellStyle.setFillForegroundColor(color);
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
    }

    public void setCellStyle(int columnIndex, int rowIndex, CellStyle cellStyle)
    {
        Row row = sheet.getRow(rowIndex);

        if(row != null)
        {
            Cell cell = row.getCell(columnIndex);

            if((cell != null) && (cellStyle != null))
            {
                cell.setCellStyle(cellStyle);
            }
        }
    }

    public int getColumnWidth(int columnIndex)
    {
        return sheet.getColumnWidth(columnIndex);
    }

    public void setColumnWidth(int columnIndex, int width)
    {
        sheet.setColumnWidth(columnIndex, width);
    }

    public void setHeaderStyle(int columnIndex, int rowIndex)
    {
        // TODO
        CellStyle style = sheet.getRow(0).getCell(0).getCellStyle();

        sheet.getRow(rowIndex).getCell(columnIndex).setCellStyle(style);
        sheet.setColumnWidth(columnIndex, sheet.getColumnWidth(0));
    }

    public void setDefaultStyle(int columnIndex, int rowIndex)
    {
        // TODO
        CellStyle style = sheet.getRow(1).getCell(1).getCellStyle();

        sheet.getRow(rowIndex).getCell(columnIndex).setCellStyle(style);
        sheet.setColumnWidth(columnIndex, sheet.getColumnWidth(0));
    }

    public void setEmptyStyle(int columnIndex, int rowIndex)
    {
        // TODO
        CellStyle style = sheet.getRow(12).getCell(3).getCellStyle();

        sheet.getRow(rowIndex).getCell(columnIndex).setCellStyle(style);
        sheet.setColumnWidth(columnIndex, sheet.getColumnWidth(0));
    }

    public ArrayList<ArrayList<String>> getRows(int columnBegin, int columnEnd, int rowBegin, int rowEnd)
    {
        if((rowBegin < rowEnd) && (rowEnd < getRowNumber()) && (columnBegin < columnEnd) && (columnEnd < getColumnNumber()))
        {
            ArrayList<ArrayList<String>> retTable = new ArrayList<ArrayList<String>>();

            for(int rowIndex = rowBegin; rowIndex <= rowEnd; rowIndex++)
            {
                retTable.add(new ArrayList<String>());

                for(int columnIndex = columnBegin; columnIndex <= columnEnd; columnIndex++)
                {
                    retTable.get(rowIndex - rowBegin).add(getCellString(columnIndex, rowIndex));
                }
            }

            return retTable;
        }

        return null;
    }

    public ArrayList<String> getColumnData(int columnIndex)
    {
        if((columnIndex < getColumnNumber()) && (getRowNumber() > 0))
        {
            ArrayList<String> retTable = new ArrayList<String>();

            for(int rowIndex = 0; rowIndex < getRowNumber(); rowIndex++)
            {
                retTable.add(getCellString(columnIndex, rowIndex));
            }

            return retTable;
        }

        return null;
    }

    public ArrayList<String> getRowData(int rowIndex)
    {
        if((rowIndex < getRowNumber()) && (getColumnNumber() > 0))
        {
            ArrayList<String> retTable = new ArrayList<String>();

            for(int columnIndex = 0; columnIndex < getColumnNumber(); columnIndex++)
            {
                retTable.add(getCellString(columnIndex, rowIndex));
            }

            return retTable;
        }

        return null;
    }

    public int close()
    {
        try
        {
            inFile.close();

            return 1;
        }
        catch(IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    public int saveAndClose()
    {
        try
        {
            inFile.close();

            FileOutputStream outFile = new FileOutputStream(new File(fileName));
            workbook.write(outFile);
            outFile.close();

            return 1;
        }
        catch(IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    public int createRow(int rowIndex)
    {
        sheet.createRow(rowIndex);

        Row r = sheet.getRow(rowIndex);

        for(int i = 0; i < getColumnNumber(); i++)
        {
            r.createCell(i);
        }

        return 1;
    }

    public int createColumn(int columnIndex)
    {
        int maxColumn = 0;

        for(int r = 0; r < getRowNumber(); r++)
        {
            Row row = sheet.getRow(r);

            if(row == null)
            {
                continue;
            }

            int lastColumn = getColumnNumber();

            if(lastColumn > maxColumn)
            {
                maxColumn = lastColumn;
            }

            if(lastColumn < columnIndex)
            {
                continue;
            }

            for(int x = lastColumn; x > columnIndex; x--)
            {
                Cell oldCell = row.getCell(x - 1);

                if(oldCell != null)
                {
                    Cell nextCell = row.getCell(x);
                    cloneCell(oldCell, nextCell);
                }
            }

            Cell cell = row.getCell(columnIndex);
            row.removeCell(cell);
        }

        return 0;
    }

    public void removeRow(int rowIndex)
    {
        int lastRowNum = sheet.getLastRowNum();

        if((rowIndex >= 0) && (rowIndex < lastRowNum))
        {
            sheet.shiftRows(rowIndex + 1, lastRowNum, -1);
        }

        Row removingRow = sheet.getRow(lastRowNum);

        if(removingRow != null)
        {
            sheet.removeRow(removingRow);
        }
    }

    public int removeColumn(int columnIndex)
    {
        int maxColumn = 0;

        for(int r = 0; r < getRowNumber(); r++)
        {
            Row row = sheet.getRow(r);

            if(row == null)
            {
                continue;
            }

            int lastColumn = row.getLastCellNum();

            if(lastColumn > maxColumn)
            {
                maxColumn = lastColumn;
            }

            if(lastColumn < columnIndex)
            {
                continue;
            }

            for(int x = columnIndex + 1; x < (lastColumn + 1); x++)
            {
                Cell oldCell = row.getCell(x - 1);

                if(oldCell != null)
                {
                    row.removeCell(oldCell);
                }

                Cell nextCell = row.getCell(x);

                if(nextCell != null)
                {
                    Cell newCell = row.createCell(x - 1, nextCell.getCellType());
                    cloneCell(newCell, nextCell);
                }
            }
        }

        for(int c = 0; c < maxColumn; c++)
        {
            sheet.setColumnWidth(c, sheet.getColumnWidth(c + 1));
        }

        return 1;
    }

    private static void cloneCell(Cell newCell, Cell oldCell)
    {
        newCell.setCellComment(oldCell.getCellComment());
        newCell.setCellStyle(oldCell.getCellStyle());

        switch(newCell.getCellType())
        {
        case Cell.CELL_TYPE_BOOLEAN:
        {
            newCell.setCellValue(oldCell.getBooleanCellValue());

            break;
        }

        case Cell.CELL_TYPE_NUMERIC:
        {
            newCell.setCellValue(oldCell.getNumericCellValue());

            break;
        }

        case Cell.CELL_TYPE_STRING:
        {
            newCell.setCellValue(oldCell.getStringCellValue());

            break;
        }

        case Cell.CELL_TYPE_ERROR:
        {
            newCell.setCellValue(oldCell.getErrorCellValue());

            break;
        }

        case Cell.CELL_TYPE_FORMULA:
        {
            newCell.setCellFormula(oldCell.getCellFormula());

            break;
        }
        }
    }
}
