package Model.TableOperation;

public interface TableOperation
{
	public Table run();
	public void setSourceTable(Table sourceTable);
	public int getTotalNumberOfOperation();
}
