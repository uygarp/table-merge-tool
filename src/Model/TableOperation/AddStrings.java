package Model.TableOperation;

import java.util.ArrayList;

public class AddRows extends TableOperationDecorator
{
	public AddRows(ArrayList<String> strList, TableOperation tableOperation)
	{
		super(strList, null, tableOperation);
	}

	@Override
	protected Table operation()
    {
	    if(getStrList() == null)
    	{
    		return getTargetTable();
    	}
    	
    	for(String strId : getStrList())
    	{
    		if(getSourceTable().getRowHeaders().contains(strId) && !getTargetTable().getRowHeaders().contains(strId))
    		{
    			ArrayList<String> targetAddStrList = new ArrayList<String>();
    			for(String col : getTargetTable().getColumnHeaders())
    			{
    				String str = "";
    				if(getSourceTable().getColumnHeaders().contains(col))
    				{
    					str = getSourceTable().getItem(col, strId);
    				}
    				targetAddStrList.add(str);    				
    			}
    			
    			getTargetTable().addRow(strId, targetAddStrList);
    			
    			notifyChanged("", strId, getOperationNo());
    		}
    		else
    		{
    		    notifyNotChanged("", strId, getOperationNo());
    		}
    	}
    	    	
	    return getTargetTable();
    }

    @Override
    public String operatedName()
    {
        return "Added";
    }

    @Override
    public int getNumberOfOperation()
    {
        return getStrList().size();
    }	
}
