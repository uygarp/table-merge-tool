package Model.TableOperation;

import java.util.ArrayList;

import Utilities.Lists.Lists;

public class Update extends TableOperationDecorator
{
    public Update(ArrayList<String> strList, ArrayList<String> colList, TableOperation tableOperation)
    {
        super(strList, colList, tableOperation);
    }

    public Update(TableOperation tableOperation)
    {
        super(null, null, tableOperation);
    }

    protected Table operation()
    {
        for(String col : getColList())
        {
            for(String strId : getStrList())
            {
                String target = getTargetTable().getItem(col, strId);
                String source = getSourceTable().getItem(col, strId);

                if(!source.equals("") && !target.equals(source))
                {
                    getTargetTable().setItem(col, strId, source);
                    notifyChanged(col, strId, getOperationNo());
                }
                else
                {
                    notifyNotChanged(col, strId, getOperationNo());
                }
            }
        }
        
        return getTargetTable();
    }

    @Override
    public String operatedName()
    {
        return "Updated";
    }

    @Override
    public int getNumberOfOperation()
    {
        ArrayList<String> commonColumns = Lists.getCommonItems(getColList(), getSourceTable().getColumnHeaders());
        ArrayList<String> commonRows = Lists.getCommonItems(getStrList(), getSourceTable().getRowHeaders());        
        return commonColumns.size() * commonRows.size();
    }
}
