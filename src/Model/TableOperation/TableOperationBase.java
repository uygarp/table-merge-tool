package Model.TableOperation;

public class TableOperationBase implements TableOperation
{
	private Table resultTable;
	
	public TableOperationBase(Table targetTable)
	{
		if(targetTable != null)
		{
			this.resultTable = targetTable.copy();
		}
	}

	public Table run()
	{
		return resultTable;
	}

	public void setSourceTable(Table sourceTable)
    {
	}

    @Override
    public int getTotalNumberOfOperation()
    {
        return 0;
    }
}
