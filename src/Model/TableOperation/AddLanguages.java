package Model.TableOperation;

import java.util.ArrayList;

import Model.TableOperation.Table;

public class AddColumns extends TableOperationDecorator
{
	public AddColumns(ArrayList<String> colList, TableOperation tableOperation)
	{
		super(null, colList, tableOperation);
	}

	@Override
	protected Table operation()
    {
	 	if(getColList() == null)
    	{
    		return getTargetTable();
    	}
    	
    	for(String col : getColList())
    	{
    		if(getSourceTable().getColumnHeaders().contains(col) && !getTargetTable().getColumnHeaders().contains(col))
    		{
    			ArrayList<String> targetAddColList = new ArrayList<String>();
    			for(String strId : getTargetTable().getRowHeaders())
    			{
    				String str = "";
    				if(getSourceTable().getRowHeaders().contains(strId))
    				{
    					str = getSourceTable().getItem(col, strId);
    				}
    				targetAddColList.add(str);
    			}

    			getTargetTable().addColumn(col, targetAddColList);
    			
    			notifyChanged(col, "", getOperationNo());
    		}
    		else
    		{
    		    notifyChanged(col, "", getOperationNo());
    		}
    	}
    	    	
	    return getTargetTable();
    }

    @Override
    public String operatedName()
    {
        return "Added";
    }

    @Override
    public int getNumberOfOperation()
    {
        return getColList().size();
    }
}
