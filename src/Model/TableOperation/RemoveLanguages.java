package Model.TableOperation;

import java.util.ArrayList;

public class RemoveColumns extends TableOperationDecorator
{
	public RemoveColumns(ArrayList<String> colList, TableOperation tableOperation)
	{
		super(null, colList, tableOperation);
	}

	@Override
	protected Table operation()
    {
	    if(getColList() == null)
    	{
    		return getTargetTable();
    	}

    	for(String col : getColList())
    	{
    		int colIndex = getTargetTable().getColumnHeaders().indexOf(col);
    		if(colIndex != -1)
    		{
    			getTargetTable().removeColumn(colIndex);
    			
    			notifyChanged(col, "", getOperationNo());
    		}
    		else
    		{
    		    notifyNotChanged(col, "", getOperationNo());
    		}
    	}
    	
    	return getTargetTable();
    }

    @Override
    public String operatedName()
    {
        return "Removed";
    }

    @Override
    public int getNumberOfOperation()
    {
        return getColList().size();
    }
}