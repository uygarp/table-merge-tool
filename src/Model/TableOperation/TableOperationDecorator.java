package Model.TableOperation;

import java.util.ArrayList;
import java.util.Arrays;

import Model.FileAccessor.ExcelSheet;
import Utilities.Observer.Observable;
import Utilities.Observer.Observer;

public abstract class TableOperationDecorator implements TableOperation, Observable
{
	protected TableOperation tableOperation;
	private ArrayList<String> strList;
	private ArrayList<String> colList;
	private Table sourceTable;
	private ExcelSheet sheet;
	private Table targetTable;
	private ArrayList<Observer> observerList;	
	private static int numberOfOperation;
	
	
	public TableOperationDecorator(ArrayList<String> strList, ArrayList<String> colList, TableOperation tableOperation)
	{
		this.strList = strList;
		this.colList = colList;
		this.tableOperation = tableOperation;
		observerList = new ArrayList<Observer>();
		numberOfOperation = 0;
	}
	
	public ArrayList<String> getStrList()
	{
		return strList;
	}
	
	public ArrayList<String> getColList()
	{
		return colList;
	}
	
	public Table getSourceTable()
	{
		return sourceTable;
	}
	
	private void setTargetTable(Table targetTable)
	{
		this.targetTable = targetTable;
	}
	
	public Table getTargetTable()
	{
		return targetTable;
	}

	public ExcelSheet getTargetSheet()
	{
		return sheet;
	}

	public void setSourceTable(Table sourceTable)
	{
		tableOperation.setSourceTable(sourceTable);
	    this.sourceTable = sourceTable;
	}
	
	public Table run()
	{
	    if(getSourceTable() == null || getSourceTable() == null)
	    {
	        return null;
	    }
	    
	    setTargetTable(tableOperation.run());
	    Table resultTable = operation();
	    resultTable.save();
	    return resultTable;
	}
	
	protected abstract Table operation();
	public abstract String operatedName();
	protected abstract int getNumberOfOperation();
	
	public int getTotalNumberOfOperation()
	{
	    return getNumberOfOperation() + tableOperation.getTotalNumberOfOperation();
	}
	
	protected int getOperationNo()
	{
	    return ++numberOfOperation;
	}

	public void notifyNotChanged(String colID, String strID, int operationNo)
	{
	    notifyStatus("Checked", colID, strID, operationNo);
	}
	
	protected void notifyChanged(String colID, String strID, int operationNo)
	{
	    notifyStatus(operatedName(), colID, strID, operationNo);
	}
	
	private void notifyStatus(String operatedName, String colID, String strID, int operationNo)
	{
	    notifyObservers(this, new ArrayList<String>(Arrays.asList(operatedName, colID, strID, Integer.toString(operationNo))));
	}
	
	@Override
    public void registerObserver(Observer observer)
    {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer)
    {
        observerList.remove(observer);
    }

    @Override
    public void notifyObservers(Object sender, Object data)
    {
        for(Observer observer : observerList)
        {
            observer.update(sender, data);
        }
    }
}
