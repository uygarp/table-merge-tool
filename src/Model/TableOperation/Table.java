package Model.TableOperation;

import Model.FileTableAccessor.*;

import java.util.ArrayList;


public class Table {
    private FileTableAccessor fileAccessor;
    private ArrayList<ArrayList<String>> tableData;
    private ArrayList<String> rowHeaders;
    private ArrayList<String> columnHeaders;
    private String fileName;

    public Table(ArrayList<ArrayList<String>> tableData, ArrayList<String> columnHeaders, ArrayList<String> rowHeaders)
    {
        if((tableData == null) || (rowHeaders == null) || (columnHeaders == null))
        {
            this.tableData = null;
            this.rowHeaders = null;
            this.columnHeaders = null;

            return;
        }

        this.tableData = tableData;
        this.rowHeaders = rowHeaders;
        this.columnHeaders = columnHeaders;
    }

    public Table(String fileName)
    {
        this.fileName = fileName;
        fileAccessor = (new FileTableAccessorFactory()).getFileTableAccessor(fileName);
        if(!(fileAccessor instanceof NullTableAccessor))
        {
            tableData = fileAccessor.getTableData();
            columnHeaders = fileAccessor.getColumnHeaders();
            rowHeaders = fileAccessor.getRowHeaders();
        }
    }
    
    public void enableFileWrite(boolean enable)
    {
    	if(enable)
    	{
    		if(fileAccessor instanceof NullTableAccessor)
    		{
    			fileAccessor = new ExcelTableAccessor(fileName);
    		}
    	}
    	else
    	{
    		if(!(fileAccessor instanceof NullTableAccessor))
    		{
    			fileAccessor = new NullTableAccessor();
    		}
    	}
    }

    private void setFileTableAccessor(FileTableAccessor fileAccessor)
    {
        this.fileAccessor = fileAccessor;
    }

    public ArrayList<String> getRowHeaders()
    {
        return rowHeaders;
    }

    public ArrayList<String> getColumnHeaders()
    {
        return columnHeaders;
    }

    public ArrayList<ArrayList<String>> getDataTable()
    {
        return tableData;
    }

    public String getItem(String column, String row)
    {
        int columnIndex = columnHeaders.indexOf(column);
        int rowIndex = rowHeaders.indexOf(row);
        String item = new String();

        if((columnIndex != -1) && (rowIndex != -1))
        {
            item = tableData.get(rowIndex).get(columnIndex);
        }

        return item;
    }

    public void setItem(String column, String row, String value)
    {
        int columnIndex = columnHeaders.indexOf(column);
        int rowIndex = rowHeaders.indexOf(row);

        if((columnIndex != -1) && (rowIndex != -1))
        {
            tableData.get(rowIndex).set(columnIndex, value);
            fileAccessor.setTableItem(columnIndex, rowIndex, value);
        }
    }

    public void addColumn(String columnHeader, ArrayList<String> columnData)
    {
        if((columnHeader != "") && (rowHeaders.size() == columnData.size()))
        {
            columnHeaders.add(columnHeader);

            for (int i = 0; i < rowHeaders.size(); i++)
            {
                tableData.get(i).add(columnData.get(i));
            }

            fileAccessor.addTableColumn(columnHeader, columnData);
        }
    }

    public void addRow(String rowHeader, ArrayList<String> rowData)
    {
        if((rowHeader != "") && (columnHeaders.size() == rowData.size()))
        {
            rowHeaders.add(rowHeader);
            tableData.add(rowData);
            fileAccessor.addTableRow(rowHeader, rowData);
        }
    }

    public void removeColumn(int columnIndex)
    {
        if((columnIndex >= 0) && (columnIndex < columnHeaders.size()))
        {
            columnHeaders.remove(columnIndex);

            for (int i = 0; i < rowHeaders.size(); i++)
            {
                tableData.get(i).remove(columnIndex);
            }

            fileAccessor.removeTableColumn(columnIndex);
        }
    }

    public void removeRow(int rowIndex)
    {
        if((rowIndex >= 0) && (rowIndex < rowHeaders.size()))
        {
            rowHeaders.remove(rowIndex);
            tableData.remove(rowIndex);
            fileAccessor.removeTableRow(rowIndex);
        }
    }

    public ArrayList<String> getColumnData(int columnIndex)
    {
        ArrayList<String> columnData = new ArrayList<String>();

        for(int i = 0; i < rowHeaders.size(); i++)
        {
            columnData.add(tableData.get(i).get(columnIndex));
        }

        return columnData;
    }

    public ArrayList<String> getRowData(int rowIndex)
    {
        return tableData.get(rowIndex);
    }

    public Table copy()
    {
        ArrayList<String> tempColumnHeaders = new ArrayList<String>();
        ArrayList<String> tempRowHeaders = new ArrayList<String>();
        ArrayList<ArrayList<String>> tempTableData = new ArrayList<ArrayList<String>>();

        for(String header : columnHeaders)
        {
            tempColumnHeaders.add(header);
        }

        for(String header : rowHeaders)
        {
            tempRowHeaders.add(header);
        }

        for(int i = 0; i < tableData.size(); i++)
        {
            tempTableData.add(new ArrayList<String>());

            for(int j = 0; j < tableData.get(0).size(); j++)
            {
                tempTableData.get(i).add(tableData.get(i).get(j));
            }
        }
        
        Table tempTable = new Table(tempTableData, tempColumnHeaders, tempRowHeaders);
        tempTable.setFileTableAccessor(fileAccessor);

        return tempTable;
    }

    public void save()
    {
        fileAccessor.save();
    }
}
