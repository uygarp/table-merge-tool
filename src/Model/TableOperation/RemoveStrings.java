package Model.TableOperation;

import java.util.ArrayList;

public class RemoveRows extends TableOperationDecorator
{
	public RemoveRows(ArrayList<String> strList, TableOperation tableOperation)
	{
		super(strList, null, tableOperation);
	}
	
	@Override
    protected Table operation()
    {
    	if(getStrList() == null)
    	{
    		return getTargetTable();
    	}

    	for(String strId : getStrList())
    	{
    		int strIndex = getTargetTable().getRowHeaders().indexOf(strId);
    		if(strIndex != -1)
    		{
    			getTargetTable().removeRow(strIndex);
    			
    			notifyChanged("", strId, getOperationNo());
    		}
    		else
    		{
    		    notifyNotChanged("", strId, getOperationNo());
    		}
    	}
    	
    	return getTargetTable();
    }

    @Override
    public String operatedName()
    {
        return "Removed";
    }

    @Override
    public int getNumberOfOperation()
    {
        return getStrList().size();
    }
}
