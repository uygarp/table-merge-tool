package Model.FileTableAccessor;


public class FileTableAccessorFactory
{
    private final String[][] fileExtentionList = {
            {"xls", "Microsoft Excel Files"},
            {"xlsx", "Microsoft Excel Files"},
            {"cdx", "Cdx Files"}};
    
    public FileTableAccessor getFileTableAccessor(String fileName)
    {
        String extension = getFileExtention(fileName);
        
        if(extension.equalsIgnoreCase(getExtention(0)) || extension.equalsIgnoreCase(getExtention(1)))
        {
            return new ExcelTableAccessor(fileName);
        }
        else if(extension.equalsIgnoreCase(getExtention(2)))
        {
            return new CdxTableAccessor(fileName);
        }
        else
        {
            return new  NullTableAccessor();
        }
    }
    
    public String[][] getFileExtentions()
    {
        return fileExtentionList;
    }
    
    private String getFileExtention(String fileName)
    {
        String extension = "";

        int lastDotIndex = fileName.lastIndexOf('.');
        int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));

        if (lastDotIndex > p)
        {
            extension = fileName.substring(lastDotIndex + 1);
        }
        
        return extension;
    }
    
    private String getExtention(int index)
    {
        String extention = "";
        
        if(index < fileExtentionList.length)
        {
            extention = fileExtentionList[index][0];
        }
        
        return extention;
    }
}
