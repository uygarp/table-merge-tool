package Model.FileTableAccessor;

import java.util.ArrayList;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;

import Model.FileAccessor.ExcelSheet;
import Model.FileAccessor.ExcelSheetFactory;

public class ExcelTableAccessor extends FileTableAccessor
{
	private String fileName = "";
	protected ExcelSheet excelSheet;
	private CellStyle headerCellStyle;
	private CellStyle defaultCellStyle;
	private CellStyle emptyCellStyle;
	
	public ExcelTableAccessor(String fileName)
	{
		this.fileName = fileName;
		open();
	}

	@Override
    public ArrayList<ArrayList<String>> getTableData()
    {
		ArrayList<ArrayList<String>> tableData =  excelSheet.getRows(1, excelSheet.getColumnNumber() - 1, 1, excelSheet.getRowNumber() - 1);			
		
		return tableData;
    }

	@Override
    public ArrayList<String> getColumnHeaders()
    {
		ArrayList<String> columnHeaders =  excelSheet.getRowData(0);
		columnHeaders.remove(0); // removes "STRING_ID"
		return columnHeaders;
    }

	@Override
    public ArrayList<String> getRowHeaders()
    {
		ArrayList<String> rowHeaders =  excelSheet.getColumnData(0);
		rowHeaders.remove(0); // removes "STRING_ID"
		return rowHeaders;
    }
	
	private CellStyle getHeaderCellStyle()
	{
		if(headerCellStyle == null)
		{
			headerCellStyle = excelSheet.copyCellStyle(0, 0);
		}
		
		return headerCellStyle;
	}
	
	private CellStyle getDefaultCellStyle()
	{
		if(defaultCellStyle == null)
		{
			defaultCellStyle = excelSheet.copyCellStyle(getHeaderCellStyle());
			excelSheet.setBold(defaultCellStyle, false);
		}
		
		return defaultCellStyle;
	}
	
	
	private CellStyle getEmptyCellStyle()
	{
		if(emptyCellStyle == null)
		{
			emptyCellStyle = excelSheet.copyCellStyle(getDefaultCellStyle());
			excelSheet.setBackgroundColor(emptyCellStyle, IndexedColors.YELLOW.getIndex());
		}
		
		return emptyCellStyle;
	}
	
	private void updateItemFormat(int columnIndex, int rowIndex, String value)
	{
		CellStyle cellStyle = getDefaultCellStyle();
		
		if(columnIndex == 0 || rowIndex == 0)
		{
			cellStyle = getHeaderCellStyle();
		}
		else if(value.isEmpty() || value == "")
		{
			cellStyle = getEmptyCellStyle();
		}
		
		excelSheet.setColumnWidth(columnIndex, excelSheet.getColumnWidth(0));
		excelSheet.setCellStyle(columnIndex, rowIndex, cellStyle);
	}

	@Override
    public void setItem(int columnIndex, int rowIndex, String value)
    {
		setCell(columnIndex + 1, rowIndex + 1, value);
    }
	
	private void setHeader(int columnIndex, int rowIndex, String header)
	{
		if((columnIndex > 0 ^ rowIndex > 0) && header != "")
		{
			setCell(columnIndex, rowIndex, header);
		}
	}
	
	public void setCell(int columnIndex, int rowIndex, String value)
	{
		excelSheet.setCellString(columnIndex, rowIndex, value);
		updateItemFormat(columnIndex, rowIndex, value);
	}

	@Override
    public void addColumn(String columnHeader, ArrayList<String> columnData)
    {
		int columnIndex = excelSheet.getColumnNumber();
		setHeader(columnIndex, 0, columnHeader);
		
		for(int i = 0; i < columnData.size(); i++)
		{
			setItem(columnIndex - 1, i, columnData.get(i));
		}
    }

	@Override
    public void addRow(String rowHeader, ArrayList<String> rowData)
    {
		int rowIndex = excelSheet.getRowNumber();
		excelSheet.createRow(rowIndex);
		setHeader(0, rowIndex, rowHeader);
		
		for(int i = 0; i < rowData.size(); i++)
		{
			setItem(i, rowIndex - 1, rowData.get(i));
		}		
    }

	@Override
    public void removeColumn(int columnIndex)
    {
		excelSheet.removeColumn(columnIndex + 1);
    }

	@Override
    public void removeRow(int rowIndex)
    {
		excelSheet.removeRow(rowIndex + 1);	    
    }

	@Override
    public void open()
    {
		excelSheet = (new ExcelSheetFactory(fileName, 0)).getSheet();
    }

	@Override
    public void save()
    {
	    excelSheet.saveAndClose();
    }

	@Override
    public void close()
    {
	    excelSheet.close();
    }
}
