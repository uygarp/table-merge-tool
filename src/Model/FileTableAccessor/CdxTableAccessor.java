package Model.FileTableAccessor;

import java.util.ArrayList;

import org.w3c.dom.Element;

import Model.FileAccessor.CdxConstants;
import Model.FileAccessor.CdxFormatter;
import Model.FileAccessor.XmlDocument;

public class CdxTableAccessor extends FileTableAccessor
{
    protected XmlDocument xml;
    protected String fileName;
    protected ArrayList<Element> columnElements;
    protected ArrayList<Element> stringElements;

    public CdxTableAccessor(String fileName)
    {
        this.fileName = fileName;
        xml = new XmlDocument();
        open();
    }

    @Override
    public ArrayList<ArrayList<String>> getTableData()
    {
        ArrayList<ArrayList<String>> tableData;
        ArrayList<String> columnHeaders = getColumnHeaders();
        ArrayList<String> rowHeaders = getRowHeaders();
        
        if(columnHeaders.size() > 0 && rowHeaders.size() > 0)
        {
            tableData = new ArrayList<ArrayList<String>>();
            
            for(int i = 0; i < stringElements.size(); i++)
            {
                tableData.add(new ArrayList<String>());
                for(String col : getColumnHeaders())
                {
                    String string = stringElements.get(i).getAttribute(col);
                    tableData.get(i).add(string);
                }
            }
            
            return tableData;
        }

        return null;
    }

    @Override
    public ArrayList<String> getColumnHeaders()
    {
        if(columnElements != null)
        {
            ArrayList<String> columnHeaders = new ArrayList<String>();
            columnHeaders.add(CdxConstants.attDefault);
            for(Element column: columnElements)
            {
                columnHeaders.add(column.getAttribute(CdxConstants.attName));
            }
            
            return columnHeaders;
        }
        
        return null;
    }

    @Override
    public ArrayList<String> getRowHeaders()
    {
        if(stringElements != null)
        {
            ArrayList<String> rowHeaders = new ArrayList<String>();
            for(Element string: stringElements)
            {
                rowHeaders.add(string.getAttribute(CdxConstants.attName));
            }
            
            return rowHeaders;
        }
        
        return null;
    }

    @Override
    protected void setItem(int columnIndex, int rowIndex, String value)
    {
        if(columnIndex > -1 && rowIndex > -1)
        {
            String col = getColumnHeaders().get(columnIndex);
            Element strElement = stringElements.get(rowIndex);
            if(strElement != null)
            {
                strElement.setAttribute(col, value);
            }            
        }        
    }

    @Override
    protected void addColumn(String columnHeader, ArrayList<String> columnData)
    {
        if(!getColumnHeaders().contains(columnHeader))
        {
            Element column = xml.createElement(columnElements.get(0).getParentNode(), CdxConstants.tagColumn);
            column.setAttribute(CdxConstants.attCode, columnHeader);
            column.setAttribute(CdxConstants.attName, columnHeader);
            column.setAttribute(CdxConstants.attNative, columnHeader);
            column.setAttribute(CdxConstants.attEnabled, CdxConstants.valEnabled);
            
            columnElements.add(column);
            
            for(int i = 0; i < columnData.size(); i++)
            {
                String strData = columnData.get(i);
                if(!strData.equals(""))
                {
                    stringElements.get(i).setAttribute(columnHeader, strData);
                }
            }
        }        
    }

    @Override
    protected void addRow(String rowHeader, ArrayList<String> rowData)
    {
        for(Element element : stringElements)
        {
            if(rowHeader.equals(element.getAttribute(CdxConstants.attName)))
            {
                return;
            }
        }
        
        ArrayList<String> columnHeaders = getColumnHeaders();
        if(columnHeaders.size() == rowData.size())
        {
            Element strElement = xml.createElement(stringElements.get(0).getParentNode(), CdxConstants.tagString);
            strElement.setAttribute(CdxConstants.attName, rowHeader);
            
            for(int i = 0; i < columnHeaders.size(); i++)
            {
                strElement.setAttribute(columnHeaders.get(i), rowData.get(i));
            }            
        }
    }

    @Override
    protected void removeColumn(int columnIndex)
    {
        if(getColumnHeaders().size() > columnIndex)
        {
            Element colElement = columnElements.get(columnIndex - 1);
            String colName = colElement.getAttribute(CdxConstants.attName);
            colElement.getParentNode().removeChild(colElement);
            
            columnElements.remove(columnIndex - 1);
            
            for(Element strElement : stringElements)
            {
                strElement.removeAttribute(colName);
            }            
        }        
    }

    @Override
    protected void removeRow(int rowIndex)
    {
        if(getRowHeaders().size() > rowIndex)
        {
            Element strElement = stringElements.get(rowIndex);
            strElement.getParentNode().removeChild(strElement);
            
            stringElements.remove(rowIndex);
        }
    }

    @Override
    protected void open()
    {
        xml.openDocument(fileName);        
        Element dictionary = xml.getRootElement(CdxConstants.tagDictionary);
        
        Element columns = xml.getElement(CdxConstants.tagColumns, dictionary);
        columnElements = xml.getChildElements(columns);
        
        Element strings = xml.getElement(CdxConstants.tagRows, dictionary);
        stringElements = xml.getChildElements(strings);
    }

    @Override
    public void save()
    {
        xml.saveDocument();
        (new CdxFormatter(fileName)).format(getColumnHeaders());
    }

    @Override
    public void close()
    {
    }
}
