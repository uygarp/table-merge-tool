package Model.FileTableAccessor;

import java.util.ArrayList;


public abstract class FileTableAccessor
{
	private boolean isOpened;
	
	abstract public ArrayList<ArrayList<String>> getTableData();
	abstract public ArrayList<String> getColumnHeaders();
	abstract public ArrayList<String> getRowHeaders();
	
	abstract protected void setItem(int columnIndex, int rowIndex, String value);
	abstract protected void addColumn(String columnHeader, ArrayList<String> columnData);
	abstract protected void addRow(String rowHeader, ArrayList<String> rowData);
	abstract protected void removeColumn(int columnIndex);
	abstract protected void removeRow(int rowIndex);
	abstract protected void open();
	abstract public void save();
	abstract public void close();
	
	private void checkAndOpen()
	{
		if(!isOpened)
		{
			open();
			isOpened = true;
		}
	}
	
	public void setTableItem(int columnIndex, int rowIndex, String value)
	{
		checkAndOpen();
		setItem(columnIndex, rowIndex, value);
	}
	
	public void addTableColumn(String columnHeader, ArrayList<String> columnData)
	{
		checkAndOpen();
		addColumn(columnHeader, columnData);
	}
	
	public void addTableRow(String rowHeader, ArrayList<String> rowData)
	{
		checkAndOpen();
		addRow(rowHeader, rowData);
	}
	
	public void removeTableColumn(int columnIndex)
	{
		checkAndOpen();
		removeColumn(columnIndex);
	}
	
	public void removeTableRow(int rowIndex)
	{
		checkAndOpen();
		removeRow(rowIndex);
	}
}
