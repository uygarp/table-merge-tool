package Model;

import java.util.ArrayList;

import Model.FileTableAccessor.FileTableAccessorFactory;
import Model.TableOperation.*;
import Utilities.Observer.Observable;
import Utilities.Observer.Observer;


public class Model implements Observable, Observer
{
    private Table targetTable;
    private Table sourceTable;
    private TableOperation tableOperation;
    private ArrayList<Observer> observerList;
    
    private ArrayList<String> updateRowList;
    private ArrayList<String> updateColumnList;
    private ArrayList<String> addRowList;
    private ArrayList<String> addColumnList;
    private ArrayList<String> removeRowList;
    private ArrayList<String> removeColumnList;
    
    public Model()
    {
        observerList = new ArrayList<Observer>();
        updateRowList = new ArrayList<String>();
        updateColumnList = new ArrayList<String>();
        addRowList = new ArrayList<String>();
        addColumnList = new ArrayList<String>();
        removeRowList = new ArrayList<String>();
        removeColumnList = new ArrayList<String>();
    }
    
    public int setTargetTable(String targetFileName)
    {
        targetTable = new Table(targetFileName);
        tableOperation = new TableOperationBase(targetTable);
        return 1;
    }
    
    public Table getTargetTable()
    {
        return targetTable;
    }
    
    public int setSourceTable(String sourceFileName)
    {
        if(sourceFileName.equals(""))
        {
            return 0;
        }
        
        this.sourceTable = new Table(sourceFileName);
        return 1;
    }
    
    public Table getSourceTable()
    {
        return sourceTable;
    }
    
    public int getTotalOperationNumber()
    {
        return tableOperation.getTotalNumberOfOperation();
    }
    
    private void attachTableOperation(TableOperationDecorator tableOperationDecorator)
    {
        tableOperationDecorator.registerObserver(this);
        tableOperation = tableOperationDecorator;
    }
    
    private void attachUpdateRows(ArrayList<String> strList, ArrayList<String> colList)
    {
        attachTableOperation(new Update(strList, colList, tableOperation));
    }
    
    private void attachAddedRows(ArrayList<String> strList)
    {
        attachTableOperation(new AddRows(strList, tableOperation));
    }
    
    private void attachRemovedRows(ArrayList<String> strList)
    {
        attachTableOperation(new RemoveRows(strList, tableOperation));
    }
    
    private void attachAddedColumns(ArrayList<String> colList)
    {
        attachTableOperation(new AddColumns(colList, tableOperation));
    }
    
    private void attachRemovedColumns(ArrayList<String> colList)
    {
        attachTableOperation(new RemoveColumns(colList, tableOperation));
    }
    
    public String[][] getFileExtentions()
    {
        return (new FileTableAccessorFactory()).getFileExtentions();
    }
    
    public void attachOperations(boolean writeFile)
    {
        targetTable.enableFileWrite(writeFile);
        tableOperation = new TableOperationBase(targetTable);
        attachUpdateRows(getUpdateRowList(), getUpdateColumnList());
        attachAddedRows(getAddRowList());
        attachAddedColumns(getAddColumnsList());
        attachRemovedRows(getRemoveRowList());
        attachRemovedColumns(getRemoveColumnsList());
        tableOperation.setSourceTable(sourceTable);
    }

    public void action()
    {
        tableOperation.run();
    }
    
    @Override
    public void registerObserver(Observer observer)
    {
        observerList.add(observer);        
    }

    @Override
    public void removeObserver(Observer observer)
    {
        if(observer != null)
        {
            observerList.remove(observer);    
        }                
    }

    @Override
    public void notifyObservers(Object sender, Object data)
    {
        for(Observer observer : observerList)
        {
            observer.update(sender, data);
        }
    }

    @Override
    public void update(Object sender, Object data)
    {
        notifyObservers(sender, data);
    }
    
    public ArrayList<String> getUpdateRowList()
    {
        return updateRowList;
    }

    public void setUpdateRowList(ArrayList<String> updateRowList)
    {
        this.updateRowList = updateRowList;
    }

    public ArrayList<String> getUpdateColumnList()
    {
        return updateColumnList;
    }

    public void setUpdateColumnList(ArrayList<String> updateColumnList)
    {
        this.updateColumnList = updateColumnList;
    }

    public ArrayList<String> getAddRowList()
    {
        return addRowList;
    }

    public void setAddRowList(ArrayList<String> addRowList)
    {
        this.addRowList = addRowList;
    }

    public ArrayList<String> getAddColumnsList()
    {
        return addColumnList;
    }

    public void setAddColumnsList(ArrayList<String> addColumnsList)
    {
        this.addColumnList = addColumnsList;
    }

    public ArrayList<String> getRemoveRowList()
    {
        return removeRowList;
    }

    public void setRemoveRowList(ArrayList<String> removeRowList)
    {
        this.removeRowList = removeRowList;
    }

    public ArrayList<String> getRemoveColumnsList()
    {
        return removeColumnList;
    }

    public void setRemoveColumnsList(ArrayList<String> removeColumnsList)
    {
        this.removeColumnList = removeColumnsList;
    }
}
