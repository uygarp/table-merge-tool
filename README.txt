In order to compile and run this code, please add the following libraries to your Java Build Path:
- external_JARs\poi-3.10.1\poi-3.10.1-20140818.jar
- external_JARs\poi-3.10.1\poi-ooxml-3.10.1-20140818.jar
- external_JARs\swt-4.4-win32-win32-x86_64\swt.jar
- external_JARs\swing2swt\swing2swt.jar

NOTE:
In Eclipse;
- Go to Project -> Properties -> Java Build Path
- Open Libraries tab, and use "Add External JARs..." button to add the above jar files.